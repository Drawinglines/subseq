"""
This module includes a set of helper functions for FFR:
# Reading from database
# Pre-processing the data
# Post-processing the data
# Writing into database

Parameters and settings:
# initDate      : A reference date for converting dates to month differences
# indexDate     : start date for testing period
# stateTable    : Dictionary of state names and IDs.
"""
from __future__ import division
from collections import defaultdict
import pdb
import MySQLdb
import numpy as np
from datetime import datetime, date
from calendar import monthrange
import cPickle as pk
from tqdm import tqdm   # For progress bar
import pdb

initDate = datetime.strptime('1900-01-01' , '%Y-%m-%d')
indexDate = datetime.strptime('2008-01-01' , '%Y-%m-%d')
stateTable = {'CA':4, 'CO':5, 'NM':30, 'NDK':33, 'OK':35, 'UT':43, 'AK':50, 'WY':49, 'RAND':0}
cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com','user':'taha','password': 'taha@terraai','db': 'results'}   
def diff_month(d1):
    """
    Calculates the difference (d1-initDate) in terms of number of months in between them.
    """
    try:
        return (d1.year - initDate.year)*12 + d1.month - initDate.month
    except:
        import pdb; pdb.set_trace()
        print "Exception.  The aregument was {}.".format(d1)
        return np.NaN

def add_months(months):
    """
    Returns the date for several months after the initDate. 
    """
    month = initDate.month - 1 + months
    year = int(initDate.year + month / 12 )
    month = month % 12 + 1
    return date(year,month,monthrange(year,month)[1])  # Reporting on the last day of the month

def getLength(oil):
    """
    Calculates the length of oil time series.  
    Here, the last data point of the time series is the last non-zero production month.
    """
    N = len(oil)
    lens = [x.shape[0] for x in oil]
    for i in range(N):
        while oil[i][lens[i]-1] == 0:
            lens[i] -= 1
            if lens[i] == 0: break
    return lens

def mynum(func, a):
    ''' This function is only a way to get around the unexpected data while converting it to type specified by 'func' argument.
    You may want to write a more sophisticated function for treatment of bad entries.'''
    try:
        return func(a)
    except:
        # print('Expected float, got {} instead'.format(type(a)))
        return 0

def handleMissing(dates, oil, water, days):
    """
    This function handles the missing data points by inserting zeros in the missing months.
    """
    new_dates = np.arange(min(dates), max(dates)+1) 
    new_oil, new_water, new_days = [np.zeros(new_dates.shape) for _ in range(3)]
    new_oil[dates-min(dates)] = oil
    new_water[dates-min(dates)] = water
    new_days[dates-min(dates)] = days
    return new_dates, new_oil, new_water, new_days

def encodePools(names):
    uni = list(set(names))
    nameMap = dict(zip(uni, xrange(len(uni))))
    return [nameMap[x] for x in names]

def readFromDB(stateID=[33]):
    """
    This function reads the data from the database and converts them to a defaultdict format.  
    It also performs some mild preprocessing on the data such as convertion of the dates to diff months and 
    handling missing data with handleMissing() function.
    """
    # Establish the connection
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], 'public_data')
    cursor = conn.cursor()
    if stateID[0] == 0:   # Random selection
        query = "SELECT api,dates,oil,water,proddays,lat,longi,depth,pool_name FROM rand_ts LIMIT 5000;"
    elif stateID[0] == 4:
        query = "SELECT API_prod,dates,oil,water,proddays,lat,longi,depth,pool_name FROM bigprod_ts WHERE API_prod in (SELECT * FROM results.ca_crc_wellids);"
    else:
        query = "SELECT API_prod,dates,oil,water,proddays,lat,longi,depth,pool_name FROM bigprod_ts WHERE state in ({});".format(','.join([str(x) for x in stateID]))

    cursor.execute(query)
    row = cursor.fetchall()
    data = defaultdict(list)
    # 
    print "Reading from the database:"
    for item in tqdm(row):
        # Exclude all-zero rows
        oil = np.array([mynum(float, x) for x in item[2].split(',')])
        if oil.sum() == 0:  continue
        dates  = np.array([diff_month(datetime.strptime(x, '%Y-%m-%d')) for x in  item[1].split(',')])
        index = np.argsort(dates)
        dates = dates[index]
        water = np.array([mynum(float, x) for x in item[3].split(',')])[index]
        days = np.array([mynum(int, x) for x in item[4].split(',')])[index]
        dates, oil, water, days = handleMissing(dates, oil[index], water, days)

        data['padID'].append( item[0] )
        data['dates'].append( dates )
        data['oil'].append( oil )
        data['water'].append( water )
        data['proddays'].append( days )
        data['location'].append( np.array([mynum(float, item[5]), mynum(float, item[6])]) )
        data['depth'].append( mynum(float, item[7]) )
        data['pool'].append(item[8])

    # Coding the pool names into numerical categories
    data['pool'] = encodePools( data['pool'] )

    cursor.close()
    conn.close()
    return data

def formatForecast(shape, hat, ln, winp, jn, log):
    """
    This is a post-processing function.
    # It removes the part of forecast that is beyond the length of the actual time series.
    # The previous one does not happen anymore. For that we use hat[:ln] instead of hat.
    # Lines 138 and 141.
    # It reverses the log operation in the preprocessing step.
    # It ensures that the output is non-negative.
    """
    fore = np.nan*np.empty(shape)
    if log:
        temp = np.power(10, hat[:ln])-1
    else:
        temp = hat[:ln]
    fore[jn[1]+winp:jn[1]+winp+ln] = (temp*(temp >= 0))
    return fore

def confidence(yhat, shat):
    """
    Assuming Gaussian noise, we use the z-scores to find 0.1 and 0.9 percentiles of the forecasts.
    """
    z = 1.645
    yhat10 = yhat - z*shat[:, None] 
    yhat90 = yhat + z*shat[:, None] 
    return yhat10, yhat90

def convertData(dataIn, yhat, shat, ids, predLen, winp, winq, target, log):
    """ 
    This function converts the data to the right defaultdict format for writing into the db. 
    It reverses the preprocessing operations and uses formatForecast() to cut the time series beyond the length of actual time series.
    Also, it invokes confidence() to calculate the percentile curves.
    """
    # import pdb; pdb.set_trace()
    data = defaultdict(list)
    for (i, jn) in enumerate(tqdm(ids)):
        tlen = min(len(dataIn[target][jn[0]]) - (jn[1]+winp), winq)
        data['padID'].append(dataIn['padID'][jn[0]])
        data['date'].append( np.array( [add_months(x) for x in dataIn['dates'][jn[0]].tolist()] ) )
        data['proddays'].append(dataIn['proddays'][jn[0]])
        if log:
            data[target].append(np.power(10, dataIn[target][jn[0]])-1)
        else:
            data[target].append(dataIn[target][jn[0]])

        shape = dataIn['proddays'][jn[0]].shape
        yhat10, yhat90 = confidence(yhat, shat)
        ss = np.ones(yhat[i,:].shape)  
        data[target+'_fore'].append( formatForecast(shape, yhat[i,:], tlen, winp, jn, log) ) 
        data[target+'_fore_std'].append( formatForecast(shape, ss, tlen, winp, jn, False) ) 
        data[target+'_fore10'].append( formatForecast(shape, yhat10[i,:], tlen, winp, jn, log) ) 
        data[target+'_fore90'].append( formatForecast(shape, yhat90[i,:], tlen, winp, jn, log) ) 
    return data

def write2db(data, name, target):
    """
    This function writes the data formatted and returned by convertData() into the database. 
    """
    # Establish the connection  
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()
    
    # Drop table if exists
    query = "DROP TABLE IF EXISTS {};".format(name)
    cursor.execute(query)

    if target in ['oil', 'water']: tgtpd = target + '_pd'
    else: tgtpd = target 
    query = """CREATE TABLE {} (API_prod BIGINT, report_date_mo DATE, proddays INT, \
     {} DOUBLE, {} DOUBLE, {} DOUBLE, {} DOUBLE, {} DOUBLE);""".format(name, \
        tgtpd, tgtpd+'_fore_mean', tgtpd+'_fore_std', tgtpd+'_fore_p10', tgtpd+'_fore_p90')
    cursor.execute(query)

    # Insert the rows
    values = list()
    for (i, well) in enumerate(data['padID']):
        for j in range(len(data[target][i])):
            values.append( "('{}','{}',{},{},{},{},{},{})".format(well, data['date'][i][j],data['proddays'][i][j], \
             data[target][i][j], data[target+'_fore'][i][j], data[target+'_fore_std'][i][j], data[target+'_fore10'][i][j], data[target+'_fore90'][i][j]))

    stepSize = 1000    # Doing the queries in batches of size 1000
    nBatch = int(np.ceil(len(values)/stepSize))
    ind = [min(x*stepSize,len(values)) for x in range(nBatch+1)]
    for i in tqdm(range(nBatch)):
        query = 'INSERT INTO {} VALUES {};'.format(name,','.join(values[ind[i]:ind[i+1]]))
        query = query.replace('nan', 'NULL')
        query = query.replace('inf', 'NULL')
        cursor.execute(query)

    conn.commit()
    cursor.close()
    conn.close()

def write2pg(data, name, target):
    from terraai_preprocessing.preprocessing.pp_main import establish_db_conn, write_DB
    # Format Taha's D_L to Aamir's D_L
    if target in ['oil', 'water']: tgtpd = target + '_pd'
    else: tgtpd = target 

    output = defaultdict(list)
    for item in tqdm(xrange(len(data['padID']))):
        tlen = len(data[target][item])
        output['API_prod'].append([data['padID'][item]]*tlen)
        output['prod_days'].append(data['proddays'][item])
        output['report_date_mo'].append(data['date'][item])
        output[tgtpd].append(data[target][item])
        output[tgtpd+'_fore_mean'].append(data[target+'_fore'][item])
        output[tgtpd+'_fore_std'].append(data[target+'_fore_std'][item])
        output[tgtpd+'_fore_p10'].append(data[target+'_fore10'][item])
        output[tgtpd+'_fore_p90'].append(data[target+'_fore90'][item])

    conn, cursor = establish_db_conn()
    write_DB(conn, cursor, output,
             verbosity=False, schema='results',
             table_name=name, drop_table=True)
    conn.close()

def savePars2db(source, features, target, params, length, state): 
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()

    query = """INSERT INTO FFR_kernels VALUES ("{}","{}","{}",{},"{}","{}");""".format( \
        source, ','.join(features), target, length, ','.join("{0:.4f}".format(x) for x in params), state)
    cursor.execute(query)
    conn.commit()
    cursor.close()
    conn.close()

def checkKernelParindb(source, features, target, length, state): 
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM FFR_kernels;")
    row = cursor.fetchall()
    for item in row:
        conds = list()
        conds.append( source==item[0] )
        conds.append( target==item[2] )
        conds.append( length==int(item[3]) )
        # conds.append( state==item[5] )
        conds.append( set(features)==set(item[1].split(',')) )
        if reduce(lambda x,y: x and y, conds):
            params_db = [float(x) for x in item[4].split(',')]
            features_db = item[1].split(',')
            order = [features_db.index(x) for x in features]
            order += [o+len(features) for o in order]
            return [params_db[o] for o in order]+[params_db[-1]]
    return None

if __name__ == "__main__":
    # pdb.set_trace()
    # data = readFromDB()
    params = np.array([  5.02493592,   0.30290206,   6.19230297,   2.20992589,
         2.11517852,   4.21499677,   0.07009884,   1.44529921,
         3.20938043,  17.86920671,   5.51474997,   0.22895883,   6.52829307])
    # params = np.array([  0.05189916,  18.57176986,   1.18000321,   5.25568831,
    #      0.05166955,   0.13827583,   2.04907352,   0.19549647,
    #      0.06967513,   4.68506733,   4.36314379,   0.30220287,   0.15321623])
    cc = abs(params[:6])/np.sum(abs(params[:6]))
    params[:6] = cc
    features = ['oil', 'water', 'proddays', 'location', 'depth', 'pool']
    source = 'bigprod_ts'
    target = 'oil'
    length = 36
    # savePars2db(source, features, target, params.tolist(), length, 'NDK')
    print checkKernelParindb(source, features, target, length, 'NDK')

