"""
This module includes a set of helper functions for FFR:
# Reading from database
# Pre-processing the data
# Post-processing the data
# Writing into database

Parameters and settings:
# initDate      : A reference date for converting dates to month differences
# indexDate     : start date for testing period
# stateTable    : Dictionary of state names and IDs.
"""
from __future__ import division
from collections import defaultdict
import MySQLdb
import numpy as np
from tqdm import tqdm   # For progress bar

cnx = {'host':'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com', 'user':'taha', 'password':'taha@terraai', 'db':'results'}

def mynum(func, number):
    """
    This function is only a way to get around the unexpected data while converting it to type
    specified by 'func' argument.
    You may want to write a more sophisticated function for treatment of bad entries.
    """
    try:
        return func(number)
    except:
        # print('Expected float, got {} instead'.format(type(a)))
        return 0

def readFromDB(state_code='NDK'):
    """
    This function reads the data from the database and converts them to a defaultdict format.
    It also performs some mild preprocessing on the data such as convertion of the dates to diff
    months and handling missing data with handleMissing() function.
    """
    # Establish the connection
    conn = MySQLdb.connect(cnx['host'], cnx['user'], cnx['password'], 'results')
    cursor = conn.cursor()

    table = 'FFR_'+state_code+'_1000_36_oil'
    query = "SELECT API_prod, group_concat(oil_pd) as oil, group_concat(oil_pd_fore_mean) as fore from {} where oil_pd_fore_mean is not NULL GROUP BY API_prod;".format(table)
    cursor.execute(query)
    row = cursor.fetchall()
    data = defaultdict(list)
    print "Reading from the database:"
    for item in tqdm(row):
        # Exclude all-zero rows
        data['padID'].append(long(item[0]))
        data['oil'].append(np.array([mynum(float, x) for x in item[1].split(',')]))
        data['fore'].append(np.array([mynum(float, x) for x in item[2].split(',')]))

    cursor.close()
    conn.close()
    return data

def evaluate_error(yture, yhat):
    errs = ((yhat-yture)**2)*(yture > 0)
    norms = np.sum(yture**2)/np.sum(yture > 0)
    if (norms == 0) or (np.sum(yture == 0)/len(yture) > 0.2):
        return np.inf
    else:
        return np.sqrt(np.mean(errs/norms))

def select_wells():
    state = 'WY'
    data = readFromDB(state)
    wellids, errors = list(), list()
    for (i, wellid) in tqdm(enumerate(data['padID'])):
        err = evaluate_error(data['oil'][i], data['fore'][i])
        if err < 0.25:
            wellids.append(wellid)
            errors.append(err)
    # import pdb; pdb.set_trace()
    np.savetxt("sorted_wells_{}.csv".format(state), np.array(wellids)[np.argsort(np.array(errors))], delimiter=",")

if __name__ == "__main__":
    select_wells()
