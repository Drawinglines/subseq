from __future__ import division
import cPickle as pk
import numpy as np
from collections import defaultdict
from scipy.spatial.distance import cdist
from scipy.stats import linregress
from numpy.linalg import inv, norm
from sklearn.linear_model import Ridge
import helpers_ffr as hpf
import pdb
import warnings
warnings.filterwarnings("ignore")  # Not a very good idea!

# Parameters
win = 84
step = 6
nInduce = 300
sm = 5          # No. of last points used for detrending
cndLim = 1000     # Condition number of the kernel for the inducing parameters.
clf = Ridge(alpha=0.1)
inDate = hpf.diff_month(hpf.indexDate)

def genIndexes(lens, nTest=1000):
    ind = np.where(np.array(lens) > 2*win)[0]
    indexes = np.random.permutation(ind.shape[0])
    indTest = ind[indexes[0:nTest]]
    indTrain = ind[indexes[nTest:]]
    return indTest, indTrain

def getIndexesFile(lens, padId):
    lendict = dict(zip(padId, lens))
    # ids = np.genfromtxt('test_ids.csv', dtype=long, delimiter=',').tolist()
    ids = np.genfromtxt('test_ids_33.csv', dtype=long, delimiter=',').tolist()
    iTest = list( set(ids) & set(padId) )
    indTrainAll = list( set(padId) - set(iTest) )
    indTest = [padId.index(x) for x in iTest if lendict[x] > (win+1) ]  #  There should be at least one point to predict
    indTrain = [padId.index(x) for x in indTrainAll if lendict[x] > 2*win ]
    # pdb.set_trace()
    return np.array(indTest),np.array(indTrain)

def getLength(oil):
    N = len(oil)
    lens = [x.shape[0] for x in oil]
    for i in range(N):
        while oil[i][lens[i]-1] == 0:
            lens[i] -= 1
            if lens[i] == 0: break
    return lens

def getData(win):
    with open( "NDKNMdata.p", "rb" ) as f:
        data = pk.load( f )
    lens = getLength(data['oil'])
    # Normalize X and Y
    # data['Xpos'] = (data['Xpos'] - data['Xpos'].min())/( data['Xpos'].max() - data['Xpos'].min() )
    # data['Ypos'] = (data['Ypos'] - data['Ypos'].min())/( data['Ypos'].max() - data['Ypos'].min() )
    # Take the log of oil
    for i in range(len(data['oil'])):
        # NaNs
        data['proddays'][i][np.isnan(data['proddays'][i])] = 0
        data['oil'][i] /= data['proddays'][i]                   # This makes oil per day (oil_pd)
        data['water'][i] /= data['proddays'][i]                   # This makes water per day (water_pd)
        # Take log of oil
        data['oil'][i] = np.log10(1+data['oil'][i])         # log(1+x) to make sure that it is invertable
        data['oil'][i][~np.isfinite(data['oil'][i])] = 0 
        data['water'][i][~np.isfinite(data['water'][i])] = 0
    return data,lens

def checkMatrix(data, Kuu, inducingPoints, start, jn, params):
    if Kuu.size == 0:
        return np.array([[kernelDiagonal(params)]]), True
    point = defaultdict(list) # Sad :(
    point['padID'].append( data['padID'][jn] )
    point['dates'].append( data['dates'][jn][start:start+win] )
    point['oil'].append( data['oil'][jn][start:start+win] )
    point['water'].append( data['water'][jn][start:start+win] )
    point['proddays'].append( data['proddays'][jn][start:start+win] )
    point['year'].append( data['year'][jn] )
    point['latitude'].append( data['latitude'][jn] )
    point['longitude'].append( data['longitude'][jn] )
    Kup = kernelInducingPoints(point, 0, 0, inducingPoints, params)
    KuuNew = np.hstack((Kuu,Kup))
    KuuNew = np.vstack((KuuNew, np.hstack( (Kup.T,np.array([[Kuu[0,0]]]) ))))
    # passed = ( np.linalg.matrix_rank(KuuNew) == KuuNew.shape[0] )
    passed = ( np.linalg.cond(KuuNew) < cndLim )
    if passed:
        return KuuNew, passed
    else:
        return Kuu, passed

def selectInducingPoints(data, ind, lens, params, nInduce=nInduce):
    inds = ind[np.random.permutation(len(ind))]
    inducingPoints = defaultdict(list)
    counter, i, Kuu = 0, 0, np.array([])
    while counter < nInduce:
        start = np.random.randint(0, lens[inds[i]]-win)
        Kuu, passing = checkMatrix(data, Kuu, inducingPoints, start, inds[i], params)
        if passing:
            # create a random index between 0, lens[i]-win
            inducingPoints['padID'].append( data['padID'][inds[i]] )
            inducingPoints['dates'].append( data['dates'][inds[i]][start:start+win] )
            inducingPoints['oil'].append( data['oil'][inds[i]][start:start+win] )
            inducingPoints['water'].append( data['water'][inds[i]][start:start+win] )
            inducingPoints['proddays'].append( data['proddays'][inds[i]][start:start+win] )
            inducingPoints['year'].append( data['year'][inds[i]] )
            inducingPoints['latitude'].append( data['latitude'][inds[i]] )
            inducingPoints['longitude'].append( data['longitude'][inds[i]] )
            counter += 1
        i += 1
    return inducingPoints

def kernelInducingPoints(instance, tind, lind, inducingPoints, params):
    cO, cW, cD, sO, sW, sD,_ = params
    KO = cdist(np.array(inducingPoints['oil']), instance['oil'][lind][tind:tind+win][np.newaxis,:]);   
    KW = cdist(np.array(inducingPoints['water']), instance['water'][lind][tind:tind+win][np.newaxis,:]);  
    KD = cdist(np.array(inducingPoints['proddays']), instance['proddays'][lind][tind:tind+win][np.newaxis,:]); 
    return cO*np.exp(-sO*KO) + cW*np.exp(-sW*KW) + cD*np.exp(-sD* KD)

def kernelDiagonal(params):
    cO, cW, cD, sO, sW, sD,_ = params
    return cO + cW + cD

def fDP(dt, mat):  # fastDiagProduct
    return np.tile(dt[:,np.newaxis], (1, mat.shape[1]))*mat

def getKernelUU(inducingPoints, params):
    n = len(inducingPoints['oil'])
    Kuu = np.zeros((n,n))
    for i in range(n):
        Kuu[:,i] = kernelInducingPoints(inducingPoints, 0, i, inducingPoints, params)[:,0]
    return Kuu

def getXLast(ys):
    # ys is an list of n x 1
    n = len(ys)
    # w,b,_,_,_ = linregress(np.arange(n), np.array(ys))
    # return w*n+b
    X, y = list(), list()
    for i in range(n-sm-1):
        X.append(ys[i:i+sm])
        y.append(ys[i+sm])
    clf.fit(np.array(X), np.array(y)[:,np.newaxis])
    return clf.predict(np.array(X[-1]))[0,0]
    # return ys[-1]

def getKernelXU(data, inds, inducingPoints, params, lens, test=False):
    # Returns a nX x nU matrix, an nX x T matrix, and an array of size nX
    Kx, y, diag, xlast = list(), list(), list(), list()
    for jn in inds:
        if test:
            tind = np.where(data['dates'][jn] == inDate)[0][0]-win
            Kx.append( kernelInducingPoints(data, tind, jn, inducingPoints, params)[:,0] )
            xx = getXLast(data['oil'][jn][tind:tind+win])
            xlast.append( xx )
            diag.append(kernelDiagonal(params))
            ytemp = np.zeros((win,))
            # We predict at most 7 years after the index date
            ytemp[:min(lens[jn]-(tind+win), win)] = data['oil'][jn][tind+win:min(lens[jn],tind+2*win)]  
            y.append(ytemp)
        else:
            if lens[jn] < 2*win: continue
            times = np.arange(lens[jn]-2*win, 0, step= -step)
            for tind in times:
                Kx.append( kernelInducingPoints(data, tind, jn, inducingPoints, params)[:,0] )
                if np.isnan(Kx[-1]).sum()>0:
                    pdb.set_trace()
                # Here we remove the value of the last point from the y. 
                # xx = getXLast(data['oil'][jn][tind+win-sm:tind+win])
                xx = getXLast(data['oil'][jn][tind:tind+win])
                y.append( np.array(data['oil'][jn][tind+win:tind+2*win]) - xx )
                diag.append(kernelDiagonal(params))
                xlast.append( xx )
            # if test:
            #     pdb.set_trace()
    return np.array(Kx), np.array(y), np.array(diag), np.array(xlast)[:,np.newaxis]

def postProcess(yhat):
    slope = np.mean(yhat-yhat[:,0][:,np.newaxis], axis=1, keepdims=True)*2/(win-1)
    slope = slope * (slope > 0)
    return yhat - np.dot(slope, np.arange(win)[np.newaxis,:])

def fitcPredictor(data, indTrain, indTest, lens, params):
    sigma = params[-1]
    inducingPoints = selectInducingPoints(data, indTrain, lens, params, nInduce=nInduce)
    print "Got the inducing points"
    Kuu = getKernelUU(inducingPoints, params)
    Kuui = inv(Kuu)
    Kxu, yTrain, dg, xnothing = getKernelXU(data, indTrain, inducingPoints, params, lens, test=False)
    Kzu, yTest, xnothing, xlast = getKernelXU(data, indTest, inducingPoints, params, lens, test=True)
    dti = 1/(dg - np.sum(Kxu*(np.dot(Kuui,Kxu.T)).T,axis=1) + (sigma**2)*np.ones((dg.shape[0],) )) 
    
    yt = fDP(dti, yTrain)
    ytt =  np.dot(fDP(dti,Kxu), np.dot(inv(Kuu + np.dot(Kxu.T,fDP(dti, Kxu))), np.dot(Kxu.T, yt) ))
    yhat = np.dot(Kzu, np.dot(Kuui, np.dot(Kxu.T, yt-ytt)))
    # yhat = postProcess(yhat)
    # pdb.set_trace()
    return yhat+xlast, yTest

if __name__ == "__main__":
    np.random.seed(0)   # For reproducability
    data,lens = getData(win)
    # indTest,indTrain = genIndexes(lens, nTest=1000)
    indTest,indTrain = getIndexesFile(lens, data['padID'])
    print "Got the indexes"
    # inducingPoints = selectInducingPoints(data, indTrain, lens, nInduce=300)
    # params = [0.5, 0.5, 0.5, 1, 1, 1, 0.03]
    params = [4, 4, 3, 0.25, 4, 2.5, 0.05]
    yhat, yTest = fitcPredictor(data, indTrain, indTest, lens, params)    ###
    # pdb.set_trace()
    print norm((yhat-yTest)*(yTest>0), ord='fro')/norm(yTest, ord='fro')
    dataOut = hpf.convertData(data, yhat, indTest, win, lens)
    hpf.write2db(dataOut, 'FFR_{}_{}_{}_indexDate'.format(win//12, step, nInduce))
