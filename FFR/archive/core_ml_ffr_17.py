from __future__ import division
import cPickle as pk
import numpy as np
from collections import defaultdict
from scipy.spatial.distance import cdist
from scipy.stats import linregress
from numpy.linalg import inv, norm
from sklearn.linear_model import Ridge
from datetime import datetime, date
import helpers_ffr as hpf
import pdb
import warnings
warnings.filterwarnings("ignore")  # Not a very good idea!

# Parameters
win = 84
winp = 36
winq = 84
step = 6
nInduce = 300
sm = 5          # No. of last points used for detrending
cndLim = nInduce     # Condition number of the kernel for the inducing parameters.
clf = Ridge(alpha=0.1)
inDate = hpf.diff_month(hpf.indexDate)
testDate = hpf.diff_month(datetime.strptime('2009-01-01' , '%Y-%m-%d'))

def getLength(oil):
    N = len(oil)
    lens = [x.shape[0] for x in oil]
    for i in range(N):
        while oil[i][lens[i]-1] == 0:
            lens[i] -= 1
            if lens[i] == 0: break
    return lens

def getData(win):
    # with open( "NDKNMdata.p", "rb" ) as f:
    with open( "NDKdata.p", 'rb') as f:
        data = pk.load( f )
    lens = getLength(data['oil'])
    testable = data['testable']
    # Normalize X and Y
    # data['Xpos'] = (data['Xpos'] - data['Xpos'].min())/( data['Xpos'].max() - data['Xpos'].min() )
    # data['Ypos'] = (data['Ypos'] - data['Ypos'].min())/( data['Ypos'].max() - data['Ypos'].min() )
    # Take the log of oil
    for i in range(len(data['oil'])):
        # NaNs
        data['proddays'][i][np.isnan(data['proddays'][i])] = 0
        data['oil'][i] /= data['proddays'][i]                   # This makes oil per day (oil_pd)
        data['water'][i] /= data['proddays'][i]                   # This makes water per day (water_pd)
        # Take log of oil
        data['oil'][i] = np.log10(1+data['oil'][i])         # log(1+x) to make sure that it is invertable
        data['oil'][i][~np.isfinite(data['oil'][i])] = 0 
        data['water'][i][~np.isfinite(data['water'][i])] = 0
    return data,lens,testable

def getTrainTest(data, testable, win, step):
    """ This produces two lists.  
    Each one of them list of tuples (i, t), where i is the well index and t is the time of testing.
    """
    testIDs, trainIDs = list(), list()
    for i in range(len(data['oil'])):
        if (testDate-winp in data['dates'][i].tolist()) and (testDate in data['dates'][i].tolist()):
            testIDs.append( (i,testDate-winp-min(data['dates'][i])) )
        else:
            for t in range(min(data['dates'][i]), min(max(data['dates'][i]), testDate-winp-winq), step):
                if t+winp+winq in data['dates'][i].tolist():
                    trainIDs.append( (i,t-min(data['dates'][i])) )
                else: 
                    break
    return testIDs, trainIDs

def checkMatrix(data, Kuu, inducingPoints, jn, params):
    if Kuu.size == 0:
        return np.array([[kernelDiagonal(params)]]), True
    Kup = kernelInducingPoints(data, jn, inducingPoints, params)
    KuuNew = np.hstack((Kuu,Kup))
    KuuNew = np.vstack((KuuNew, np.hstack( (Kup.T,np.array([[Kuu[0,0]]]) ))))
    # passed = ( np.linalg.matrix_rank(KuuNew) == KuuNew.shape[0] )
    passed = ( np.linalg.cond(KuuNew) < cndLim )
    if passed:
        return KuuNew, passed
    else:
        return Kuu, passed

def selectInducingPoints(data, inds, params, nInduce=nInduce):
    inds = [inds[x] for x in np.random.permutation(len(inds)).tolist()]  # Shuffling for this purpose
    inducingPoints = list()
    counter, i, Kuu = 0, 0, np.array([])
    while counter < nInduce:
        Kuu, passing = checkMatrix(data, Kuu, inducingPoints, inds[i], params)
        if passing:
            inducingPoints.append(inds[i])
            counter += 1
        i += 1
    return inducingPoints, Kuu

def kernelInducingPoints(data, jn, inducingPoints, params):  
    cO, cW, cD, sO, sW, sD,_ = params
    KO = [cdist(data['oil'][x[0]][x[1]:x[1]+winp][None,:], data['oil'][jn[0]][jn[1]:jn[1]+winp][None,:]) for x in inducingPoints]  
    KW = [cdist(data['water'][x[0]][x[1]:x[1]+winp][None,:], data['water'][jn[0]][jn[1]:jn[1]+winp][None,:]) for x in inducingPoints]  
    KD = [cdist(data['proddays'][x[0]][x[1]:x[1]+winp][None,:], data['proddays'][jn[0]][jn[1]:jn[1]+winp][None,:]) for x in inducingPoints]  
    return cO*np.exp(-sO*np.vstack(tuple(KO))) + cW*np.exp(-sW*np.vstack(tuple(KW))) + cD*np.exp(-sD* np.vstack(tuple(KD)))

def kernelDiagonal(params):
    cO, cW, cD, sO, sW, sD,_ = params
    return cO + cW + cD

def fDP(dt, mat):  # fastDiagProduct
    return np.tile(dt[:,np.newaxis], (1, mat.shape[1]))*mat

def getXLast(ys):
    # ys is an list of n x 1
    n = len(ys)
    # w,b,_,_,_ = linregress(np.arange(n), np.array(ys))
    # return w*n+b
    X, y = list(), list()
    for i in range(n-sm-1):
        X.append(ys[i:i+sm])
        y.append(ys[i+sm])
    clf.fit(np.array(X), np.array(y)[:,np.newaxis])
    return clf.predict(np.array(X[-1]))[0]
    # return ys[-1]

def getKernelXU(data, inds, inducingPoints, lens, params, test=False):
    # Returns a nX x nU matrix, an nX x T matrix, and an array of size nX
    Kx, y, diag, xlast = list(), list(), list(), list()
    pdb.set_trace()
    for (i, jn) in enumerate(inds):
        if test:
            Kx.append( kernelInducingPoints(data, jn, inducingPoints, params)[:,0] )
            xx = getXLast(data['oil'][jn[0]][jn[1]:jn[1]+winp])
            xlast.append( xx )
            diag.append(kernelDiagonal(params))
            ytemp = np.zeros((winq,))
            # We predict at most winq after the index date
            ytemp[:min(lens[jn[0]]-(jn[1]+winq), winq)] = data['oil'][jn[0]][jn[1]+winp:min(lens[jn[0]],jn[1]+winp+winq)]  
            y.append(ytemp)
        else:
            Kx.append( kernelInducingPoints(data, jn, inducingPoints, params)[:,0] )
            if np.isnan(Kx[-1]).sum()>0:    pdb.set_trace()
            # Here we remove the value of the last point from the y. \
            xx = getXLast(data['oil'][jn[0]][jn[1]:jn[1]+winp])
            y.append( np.array(data['oil'][jn[0]][jn[1]+winp:jn[1]+winp+winq]) - xx )
            diag.append(kernelDiagonal(params))
            xlast.append( xx )
        if (i+1)%100==0:  print i
    return np.array(Kx), np.array(y), np.array(diag), np.array(xlast)[:,np.newaxis]

def postProcess(yhat):
    slope = np.mean(yhat-yhat[:,0][:,np.newaxis], axis=1, keepdims=True)*2/(win-1)
    slope = slope * (slope > 0)
    return yhat - np.dot(slope, np.arange(win)[np.newaxis,:])

def fitcPredictor(data, testIDs, trainIDs, lens, params):
    sigma = params[-1]
    inducingPoints, Kuu = selectInducingPoints(data, trainIDs, params, nInduce=nInduce)
    print "Got the inducing points"
    Kuui = inv(Kuu)
    Kxu, yTrain, dg, xnothing = getKernelXU(data, trainIDs, inducingPoints, lens, params, test=False)
    Kzu, yTest, xnothing, xlast = getKernelXU(data, testIDs, inducingPoints, lens, params, test=True)
    dti = 1/(dg - np.sum(Kxu*(np.dot(Kuui,Kxu.T)).T,axis=1) + (sigma**2)*np.ones((dg.shape[0],) )) 
    
    yt = fDP(dti, yTrain)
    ytt =  np.dot(fDP(dti,Kxu), np.dot(inv(Kuu + np.dot(Kxu.T,fDP(dti, Kxu))), np.dot(Kxu.T, yt) ))
    yhat = np.dot(Kzu, np.dot(Kuui, np.dot(Kxu.T, yt-ytt)))
    # yhat = postProcess(yhat)
    # pdb.set_trace()
    return yhat+xlast, yTest

if __name__ == "__main__":
    np.random.seed(0)   # For reproducability
    data,lens,testable = getData(win)
    # indTest,indTrain = genIndexes(lens, nTest=1000)
    testIDs, trainIDs = getTrainTest(data, testable, win, step)
    print "Got the indexes"
    # inducingPoints = selectInducingPoints(data, indTrain, lens, nInduce=300)
    # params = [0.5, 0.5, 0.5, 1, 1, 1, 0.03]
    params = [4, 4, 3, 0.25, 4, 2.5, 0.05]
    yhat, yTest = fitcPredictor(data, testIDs, trainIDs, lens, params) 
    # pdb.set_trace()
    print norm((yhat-yTest)*(yTest>0), ord='fro')/norm(yTest, ord='fro')
    dataOut = hpf.convertData(data, yhat, indTest, win, lens)
    hpf.write2db(dataOut, 'FFR_{}_{}_{}_indexDate'.format(win//12, step, nInduce))
