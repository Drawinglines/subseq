"""
This file implements the core machine learning part of the FFR algorithm.
The db interface and auxilliary functions are in 'helpers_ffr' module.

The main function in this file is `ffr()`.

Note that the FFR algorithm cannot be understood without the mathematical
 description provided in the following link:
http://terra.ai:8090/display/forteam/FFR+and+SSM

Parameters and settings:
# STATE:    the state to be analyzed.
# WINP:     length of the sequence used for training
# WINQ:     max length of the testing period
# N_INDUCE:  number of inducing points in FITC
# SM:       number of time steps used for linear regression in
#           finding the bias term.
# TEST_DATE: start date for testing period
"""

from __future__ import division
import warnings
from collections import OrderedDict, Counter
from multiprocessing import Pool
from datetime import datetime

import numpy as np
from numpy.linalg import inv, norm
from scipy.spatial.distance import cdist
from scipy.linalg import eigh
from scipy.stats import mode
from sklearn.linear_model import Ridge, TheilSenRegressor
from sklearn.cluster import SpectralClustering
from sklearn.neighbors import KNeighborsClassifier
from tqdm import tqdm   # For progress bar

import helpers_ffr as hpf

warnings.filterwarnings("ignore")  # Not a very brilliant idea!

STATE = 'NDK'
# target = 'oil'
DIMS = ['oil', 'water', 'wor', 'cumoil', 'cumwater', 'proddays']
FEATURES = OrderedDict(zip(['oil', 'water', 'proddays', 'location', 'depth', 'pool'],\
                        ['ts', 'ts', 'ts', 'vector', 'scalar', 'category']))
WINP = 36
WINQ = 84
LWIN = 5
STEP = 6
REMOVE_SPIKES = False
N_INDUCE = 1000
SM = 10              # No. of last points used for detrending
CNDLIM = N_INDUCE     # Condition number of the kernel for the inducing parameters.
CLF = Ridge(alpha=0.1)
TEST_DATE = hpf.diff_month(datetime.strptime('2009-01-01', '%Y-%m-%d'))

def load_data(target, state=STATE):
    """
    Globally Loading the Data.
    It indends to improve efficiency of the algorithms that use the large `data` variable.
    """
    global data, LENS
    data = hpf.readFromDB(stateID=[hpf.stateTable[state]])
    LENS = hpf.getLength(data['oil'])
    # Check if the target variable is correctly selected.
    assert target in DIMS, "'{}' is not a valid target variable name.".format(target)

def preprocess(target):
    """ This function implements the most basic preprocessing for the data.
    It does the followings:
    # Makes sure that all of the values are finite.
    # Converts monthly oil and water to daily via dividing them by `proddays`.
    # Takes the log of oil and water.

    NOTE: This function is the only function that changes the global variable `data`
    after 'load_data()'.
    """
    if target not in FEATURES.keys():
        FEATURES[target] = 'ts'
    for i in range(len(data['oil'])):
        # For multi-dimensional forecasts
        if target is 'wor':
            data['wor'].append(data['water'][i]/data['oil'][i])
        elif target is 'cumoil':
            data['cumoil'][i].append(np.cumsum(data['oil'][i]))
        elif target is 'cumwater':
            data['cumwater'][i].append(np.cumsum(data['cumwater'][i]))
        # Main variables
        data['proddays'][i][np.isnan(data['proddays'][i])] = 0
        data['oil'][i] /= data['proddays'][i]        # This makes oil per day (oil_pd)
        data['water'][i] /= data['proddays'][i]      # This makes water per day (water_pd)
        # Take log of oil
        data['oil'][i] = np.log10(1+data['oil'][i])  # log(1+x) to make sure that it is invertable
        data['oil'][i][~np.isfinite(data['oil'][i])] = 0
        data['water'][i] = np.log10(1+data['water'][i]) # log(1+x) to make sure invertablity
        data['water'][i][~np.isfinite(data['water'][i])] = 0
        data[target][i][~np.isfinite(data[target][i])] = 0

def univariate_lin_reg(x, y):
    """
    Performs simple linear regression and reports the intercept.
    The inputs x and y are column vectors.
    """
    lam = 0.001
    r_term = sum(y*x)/len(x) - y.mean()/x.mean()*(np.mean(x**2)+lam)
    l_term = x.mean() - (np.mean(x**2)+lam)/x.mean()
    return r_term/l_term

def remove_spikes(target, inds):
    """
    This function removes the large spikes from the data.
    Inputs:
      'target':  The target quantity, such as 'oil' or 'water'.
      'inds':    Indecies of the wells that this function will process.
    Output:
      Changes the global variable 'data'.

    NOTE: This function changes the global data.
    """
    # Finding the unique indecies in the selected p,q sequences.
    uind = sorted(list(set([x[0] for x in inds])))
    # For every well do the following:
    for jn in tqdm(uind):
        # Save a copy of the time series
        old_ts = data[target][jn]
        # make it before 2009
        # t_temp = np.where(data['dates'][jn] == TEST_DATE)[0]
        # tmax = len(data['oil'][jn])-LWIN
        # if len(t_temp)>0:
        #     if len(data['oil'][jn])-t_temp[0] > LWIN:
        #         tmax = t_temp[0]
        # For every timestamp in the interval [LWIN, Len-LWIN] do
        for t in range(LWIN, len(data[target][jn])-LWIN):
            # if t==6: import pdb; pdb.set_trace()
            point = data[target][jn][t]
            # Get the left estimate
            l_est = univariate_lin_reg(np.arange(-LWIN, 0), old_ts[t-LWIN:t])
            # Get the right estimate
            r_est = univariate_lin_reg(np.arange(1, LWIN+1), old_ts[t+1:t+LWIN+1])
            # if min(abs(l_est-point), abs(r_est-point))/(l_est+r_est) > 0.2:
            # If the normalized amount of jump is larger than a ratio, replace
            # it by the average of the estimates.
            if min(max(point-l_est, 0), max(point-r_est, 0))/(l_est+r_est) > 0.2:
                data[target][jn][t] = (l_est+r_est)/2

def notfailed(i, t):
    """
    A simple way to see if the well has failed or not.
    """
    if sum(data['oil'][i][t:t+WINP] == 0)/WINP > 0.3:
        return False
    else:
        return True

def get_train_test(step=STEP):
    """ This function produces two lists.
    Each one of them list of tuples (i, t), where i is the well index and t is the time of testing.

    The first list is the list of all indexes for the test wells plus the testing time for them.
    The second one is the list of training (i, t).
    This function ensures that the testing criteria given in the confluence page are met.
    """
    test_ids, train_ids, pred_lens = list(), list(), list()
    for i in range(len(data['oil'])):
        if (TEST_DATE-WINP in data['dates'][i].tolist()) and \
         (TEST_DATE in data['dates'][i].tolist()):
            t = TEST_DATE-WINP-min(data['dates'][i])
            pred_len = min(LENS[i], t+WINQ+WINP) - (t+WINP)       # How long we can predict
            if (pred_len > 0) and notfailed(i, t):
                test_ids.append((i, t))
                pred_lens.append(pred_len)
        else:
            for t in range(min(data['dates'][i]), min(max(data['dates'][i]), TEST_DATE-WINP-WINQ), step):
                if t+WINP+WINQ in data['dates'][i].tolist():
                    t0 = t+WINP-min(data['dates'][i])
                    # Making sure that the well has not failed in the prediction phase.
                    if len(np.where(data['oil'][i][t0:t0+WINQ] == 0)[0]) < SM:
                        train_ids.append((i, t-min(data['dates'][i])))
                else:
                    break
    return test_ids, train_ids, pred_lens

def check_matrix(Kuu, inducing_points, jn, params):
    """
    This function checks if adding the new inducing point identified by 'jn' to the
    set of 'inducing_points' will guarantee that the condition number of their joint
    kernel matrix is less than 'CNDLIM'.  This is aimed at making FFR with FITC more
    stable.
    """
    if Kuu.size == 0:
        return np.array([[kernel_diagonal(params)]]), True
    Kup = kernel_inducing_points(jn, inducing_points, params)
    Kuu_new = np.hstack((Kuu, Kup))
    Kuu_new = np.vstack((Kuu_new, np.hstack((Kup.T, np.array([[Kuu[0, 0]]])))))
    passed = (np.linalg.cond(Kuu_new) < CNDLIM)
    if passed:
        return Kuu_new, passed
    else:
        return Kuu, passed

def select_inducing_points(inds, params, n_induce=N_INDUCE):
    """
    FITC needs a set of inducing points. Selecting those points randomly usually
    creates numerical stability problems.  This function implements a simple loop
    that selects a point if its addition to the set of currently selected points
    preserves the low condition number of the kernel matrix.

    The inducing points are selected from the training data indexes specified in
    'ind'.
    """
    inds = [inds[x] for x in np.random.permutation(len(inds)).tolist()]  # Shuffling
    inducing_points = list()
    counter, i, Kuu = 0, 0, np.array([])
    print "Finding the inducing points..."
    for i in tqdm(xrange(len(inds))):
        Kuu, passing = check_matrix(Kuu, inducing_points, inds[i], params)
        if passing:
            inducing_points.append(inds[i])
            counter += 1
        if counter == n_induce:
            break
    return inducing_points, Kuu

def kernel_inducing_points(jn, inducing_points, params):
    """
    Computes the kernel between a new point 'jn' and the 'inducing_points'.
    The result is a vertical vector of size n by 1 where n = |inducing_points|.
    """
    K = np.zeros((len(inducing_points), len(FEATURES.keys())))
    for (j, item) in enumerate(FEATURES.keys()):
        for (i, x) in enumerate(inducing_points):
            if FEATURES[item] == 'ts':
                K[i, j] = cdist(data[item][x[0]][x[1]:x[1]+WINP][None, :],\
                 data[item][jn[0]][jn[1]:jn[1]+WINP][None, :])[0, 0]
            elif FEATURES[item] == 'vector':
                K[i, j] = cdist(data[item][x[0]][None, :], data[item][jn[0]][None, :])
            elif FEATURES[item] == 'scalar':
                K[i, j] = abs(data[item][x[0]] - data[item][jn[0]])
            elif FEATURES[item] == 'category':
                K[i, j] = (data[item][x[0]] != data[item][jn[0]])
    return np.sum(np.array(params[0])[None, :]*np.exp(-np.array(params[1])[None, :]*K),\
     axis=1, keepdims=True)

# def kernel(jn, kn, params):
#     """
#     Computes the kernel between a new point 'jn' and 'kn'.
#     """
#     K = np.zeros((len(FEATURES.keys(),)))
#     for (j, item) in enumerate(FEATURES.keys()):
#         if FEATURES[item] == 'ts':
#             K[j] = cdist(data[item][kn[0]][kn[1]:kn[1]+WINP][None, :],\
#                 data[item][jn[0]][jn[1]:jn[1]+WINP][None, :])[0, 0]
#         elif FEATURES[item] == 'vector':
#             K[j] = cdist(data[item][kn[0]][None, :], data[item][jn[0]][None, :])
#         elif FEATURES[item] == 'scalar':
#             K[j] = abs(data[item][kn[0]] - data[item][jn[0]])
#         elif FEATURES[item] == 'category':
#             K[j] = (data[item][kn[0]] != data[item][jn[0]])
#     return np.sum(np.array(params[0])*np.exp(-np.array(params[1])*K))

def kernel_diagonal(params):
    """
    Get the maximum value of the kernel.
    """
    return sum(params[0])

def fdp(dt, mat, l_or_r='L'):  # fastDiagProduct
    """
    A simple function to perform matrix multiplication when the first matrix 'dt' is diagonal.
    """
    if l_or_r=='L':
        return np.tile(dt[:, None], (1, mat.shape[1]))*mat
    else:
        return mat*np.tile(dt[:, None], (1, mat.shape[0]))

def get_k_xy_one(inp):
    """
    This function computes the kernel between inducing points and a single target point.
    """
    jn, inducing_points, params, target = inp
    Kx = kernel_inducing_points(jn, inducing_points, params)[:, 0]
    diag = kernel_diagonal(params)
    xlast = data[target][jn[0]][jn[1]+WINP-1]  # Now we use the last point in the training set
    y_temp = np.array(data[target][jn[0]][jn[1]+WINP-1:jn[1]+WINP+WINQ]) - xlast
    y = np.zeros((WINQ+1,))
    y[-y_temp.shape[0]:] = y_temp
    return Kx, y, diag, xlast

def get_kernel_xu_parallel(target, inds, inducing_points, params, seq=False):
    """
    Parallel implementation of kernel computation function.
    This function build the kernels between the training and testing data points and
    the inducing points 'Kx'.  It also extracts the qt time series (here called y)
    time series.  It does this for both oil (yO) and water (yW). It also outputs the
    best linear estimator for the next month oil and water production using `getXLast()`
    function. Finally, it calculates the diagonal elements of the kernel matrix for the
    input data using the `kernelDiagonal()` function.

    Returns a nX x nU matrix, an nX x T matrix, and an array of size nX
    """
    if seq:
        results = list()
        for (i, jn) in enumerate(inds):
            results.append(get_k_xy_one((jn, inducing_points, params, target)))
    else:
        inputs = list()
        pool = Pool()
        for (i, jn) in enumerate(inds):
            inputs.append((jn, inducing_points, params, target))
        results = pool.map(get_k_xy_one, inputs)
        pool.close()

    Kx = np.array([x[0] for x in results])
    y = np.array([x[1] for x in results])
    diag = np.array([x[2] for x in results])
    xlast = np.array([x[3] for x in results])[:, None]
    return Kx, y, diag, xlast

def fitc_predictor(target, test_ids, train_ids, params, sigma, n_indc=N_INDUCE):
    """
    This function is the implemetation of the core FITC algorithm. It performs the following steps:
    # Selects a set of inducing points.
    # Calculates the training and testing kernels with the inducing points.
    # Runs FITC approximation for (1) oil, (2) water, and obtains the predictive variance.

    In this function, I have closely followed the description in the following paper:

        Chalupka, K., Williams, C. K., & Murray, I. (2013).
        A framework for evaluating approximation methods for Gaussian process regression.
        The Journal of Machine Learning Research, 14(1), 333-350.

    It is strongly recommended to read it before trying to understand the math here.
    """
    inducing_points, Kuu = select_inducing_points(train_ids, params, n_indc)
    print "Got the inducing points"
    Kuui = inv(Kuu)
    Kxu, y_train, dg, _ = get_kernel_xu_parallel(target, train_ids, inducing_points, params)
    Kzu, y_test, _, xlast = get_kernel_xu_parallel(target, test_ids, inducing_points, params)
    dti = 1/(dg - np.sum(Kxu*(np.dot(Kuui, Kxu.T)).T, axis=1) + (sigma**2)*np.ones((dg.shape[0], )))
    print "Matrix multiplications..."
    # import pdb
    # pdb.set_trace()
    # Mean
    yt = fdp(dti, y_train)
    ytt = np.dot(fdp(dti, Kxu), np.dot(inv(Kuu + np.dot(Kxu.T, fdp(dti, Kxu))), np.dot(Kxu.T, yt)))
    yhat = np.dot(Kzu, np.dot(Kuui, np.dot(Kxu.T, yt-ytt)))
    # Variance
    yt = fdp(dti, np.dot(Kzu, np.dot(Kuui, Kxu.T)).T)
    ytt = np.dot(fdp(dti, Kxu), np.dot(inv(Kuu + np.dot(Kxu.T, fdp(dti, Kxu))), np.dot(Kxu.T, yt)))
    kzz = np.array([kernel_inducing_points(x, [x], params)[0, 0] for x in test_ids])
    vhat = kzz - np.diag(np.dot(Kzu, np.dot(Kuui, np.dot(Kxu.T, yt-ytt))))
    shat = np.sqrt(vhat)
    return (yhat+xlast)[:, 1:], (y_test+xlast)[:, 1:], shat

def knn(params, pred_lens, test_ids, train_ids, cluster_test, knn_n):
    kernel, _, _, _  = get_kernel_xu_parallel('oil', train_ids, test_ids, params)
    labels = cluster_test[np.argsort(kernel, axis=1)[:, -knn_n:]]
    return [Counter(labels[i, :].tolist()).most_common()[0][0] for i in range(len(train_ids))]

def spectral_clustering(params, pred_lens, test_ids, n_cluster):
    kernel, _, _, _  = get_kernel_xu_parallel('oil', test_ids, test_ids, params)
    return SpectralClustering(n_clusters=3, affinity='precomputed').fit(kernel).labels_

def clustering_wells(test_ids, train_ids, params, pred_lens, n_cluster, knn_n=5):
    if n_cluster == 1:
        return [test_ids], [train_ids], [pred_lens]
    cluster_test = spectral_clustering(params, pred_lens, test_ids, n_cluster)
    print "Test wells clustered."
    cluster_train = knn(params, pred_lens, test_ids, train_ids, cluster_test, knn_n)
    print "Train wells clustered."
    test_ids_out, train_ids_out, pd_lens = list(), list(), list()
    for n in range(n_cluster):
        test_ids_out.append([test_ids[i] for i in range(len(test_ids)) if cluster_test[i] == n])
        train_ids_out.append([train_ids[i] for i in range(len(train_ids)) if cluster_train[i] == n])
        pd_lens.append([pred_lens[i] for i in range(len(test_ids)) if cluster_test[i] == n])
    return test_ids_out, train_ids_out, pd_lens

def evaluate_error(yture, yhat, measure='mean'):
    """
    Evaluation of the error of the forecasting.
    """
    errs = np.sum(((yhat-yture)**2)*(yture > 0), axis=1)
    norms = np.sum(yture**2, axis=1)
    nnz = np.where(norms != 0)[0]
    if measure == 'median':
        return np.median(np.sqrt(errs[nnz])/np.sqrt(norms[nnz]))
    elif measure == 'weighted':
        weights = norms/sum(norms)
        return np.sqrt(sum(weights[nnz]*errs[nnz]/norms[nnz]))
    else:
        return np.sqrt(np.mean(errs[nnz]/norms[nnz]))

def ffr(target='oil', state=STATE, table_name=None, params=None, n_cluster=1):
    """
    This is the top function that calls functions in the right sequence and prints the proper output
    in the screen.  Please follow the print functions' output to see this function's objective.
    """
    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Starting FFR for {} on state {}.".format(target, state))
    log = True if target in ['oil', 'water'] else False

    np.random.seed(0)   # For reproducability
    load_data(target, state)
    print "Pre-processing the data."
    preprocess(target)
    test_ids, train_ids, pred_lens = get_train_test(STEP)
    print "Got the indexes"
    if REMOVE_SPIKES:
        print "Removing spikes..."
        remove_spikes(target, test_ids+train_ids)

    if params is None:
        params = [[0.31, 0.26, 0.66, 0, 0, 0], [0.82, 7.19, 4.75, 1, 1, 1]]  # New tuned parameters
        sigma = 2.29
    else:
        sigma = params[-1]
        dim = int((len(params)-1)/2)
        params = [params[:dim], params[dim:2*dim]]

    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Running the FFR with GP/FITC algorithm.")
    yhat, y_test, shat = list(), list(), list()
    test_ids, train_ids, pd_lens = clustering_wells(test_ids, train_ids, params, pred_lens, n_cluster, 5)
    for n in range(n_cluster):
        yhat_temp, y_test_temp, shat_temp = fitc_predictor(target, test_ids[n], train_ids[n], params, sigma)
        yhat.append(yhat_temp)
        y_test.append(y_test_temp)
        shat.append(shat_temp)
    # Combine the yhats
    yhat, y_test, shat = np.vstack(tuple(yhat)).astype(float), np.vstack(tuple(y_test)).astype(float), np.concatenate(tuple(shat)).astype(float)
    pred_lens = [item for sublist in pd_lens for item in sublist]  # Consistent with the old convention.
    test_ids = [item for sublist in test_ids for item in sublist]
    print "Noramlized Median for {}:\t{}.".format(target, evaluate_error(y_test, yhat, 'median'))
    print "Noramlized RMSE for {}:\t{}.".format(target, evaluate_error(y_test, yhat, 'mean'))
    print "Noramlized W-RMSE for {}:\t{}.".format(target, evaluate_error(y_test, yhat, 'weighted'))
    print "Postprocessing the data..."
    data_out = hpf.convertData(data, yhat, shat, test_ids, pred_lens, WINP, target, log)
    if table_name is None:
        table_name = 'FFR_{}_{}_{}_{}'.format(state, N_INDUCE, WINP, target)

    print "Writing to db in batches..."
    hpf.write2pg(data_out, table_name, target)
    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Finished FFR on state {}.".format(state))
    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Forecasts are in table {}.".format(table_name))

if __name__ == "__main__":
    PAR = [0.251, 0.015, 0.309, 0.110, 0.105, 0.210,
           0.070, 1.445, 3.209, 17.869, 5.515, 0.229, 6.528]
    ffr('oil', STATE, 'test_NDK', PAR, 1)
    # params = [[0.0021, 0.7355, 0.0467, 0.2082, 0.0020, 0.0055],\
    # [2.0491, 0.1955, 0.0697, 4.6851, 4.3631, 0.3022]]
    # sigma = 0.1532
    # params = [[0.3331, 0.1110, 0.0104, 0.0042, 0.0515, 0.0018, 0.1690],\
    # [0.1181, 0.0102, 0.0168, 0.0089, 0.0104, 0.0870, 0.0171]]
    # params = [[0.017, 0.056, 0.006, 0.009, 0.369, 0.457, 0.087],\
    #           [0.226, 0.060, 0.175, 0.370, 4.106, 0.077, 1.787]]
    # sigma = 0.5
