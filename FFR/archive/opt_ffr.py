from __future__ import division
from multiprocessing import Pool
import cPickle as pk
import pdb

import numpy as np
from numpy.linalg import inv
from scipy.optimize import minimize

# import core_ml_ffr as ml
import core_ml_nn as nn
import helpers_pg as hpf

def select_good_wells(train_ids, N=1000):
    """
    This function select a set of wells that have not failed during training and testing periods.
    """
    ind, cntr = list(), 0
    for ii in train_ids:
        eq_z = [x == 0 for x in ml.data['oil'][ii[0]][ii[1]:ii[1]+ml.WINP+ml.WINQ]]
        if sum(eq_z) == 0:
            ind.append(ii)
            cntr += 1
        if cntr > N:
            break
    return ind

def neg_log_likelihood(params, ind, pred_lens, target):
    """
    This function computes the negative log-likelihood of 1000 training points given the parameters.
    The goal is to find the best hyper-parameters by searching over them.
    """
    sigma = params[-1]
    L = int((params.shape[0]-1)/2)
    par = [params[:L].tolist(), params[L:2*L].tolist()]
    Kxx, y, _, _ = ml.get_kernel_xu_parallel(target, ind, ind, pred_lens, par, False, True)
    n = Kxx.shape[0]
    (_, logdet) = np.linalg.slogdet(Kxx+(sigma**2)*np.eye(n))
    return np.trace(np.dot(y.T, np.dot(inv((sigma**2)*np.eye(n)+Kxx), y)))/ml.WINQ + logdet

def opt_variance_rand(train_ids, pred_lens, target, N=50, fdim=6): # 50
    """
    Randomized search for the best hyperparameters using the function neg_log_likelihood().
    This function is independent of the function ffr() and it should be run separately.
    """
    params, results = list(), list()
    dim = fdim*2+1
    for _ in range(N):
        par = np.array([np.exp(np.random.uniform(-3, 3)) for _ in range(dim)])
        res = neg_log_likelihood(par, train_ids, pred_lens, target)
        params.append(par)
        results.append(res)
    ind = np.argsort(results)[:5]
    return np.mean(np.array(params)[ind, :], axis=0)

def opt_sub_func(inp):
    """
    This function reorders the input and invokes the optimization routine. Then it extracts the
    right output.
    """
    init, arg2 = inp
    opt_res = minimize(neg_log_likelihood, init, arg2, method='Nelder-Mead', options={'maxiter':2})
    return opt_res['x'], opt_res['fun']

def opt_variance_zero(train_ids, pred_lens, target, N=16, fdim=7): # 50
    """
    Randomized search for the best hyperparameters using the function neg_log_likelihood().
    This function is independent of the function ffr() and it should be run separately.
    """
    dim = fdim*2+1
    inputs = list()
    for _ in range(N):
        init = np.array([np.exp(np.random.uniform(-3, 3)) for _ in range(dim)])
        inputs.append((init, (train_ids, pred_lens, target)))
    pool = Pool(N)
    outputs = pool.map(opt_sub_func, inputs)
    params = [x[0] for x in outputs]
    results = [x[1] for x in outputs]

    ind = np.argsort(results)[:1]
    return np.mean(np.array(params)[ind, :], axis=0)

def opt_kernel_pars_ml(target='oil', n_well=1000):
    """
    Optimize the kernel parameters using maximum likelihood.
    """
    np.random.seed(0)   # For reproducability
    ml.load_data(target)
    ml.preprocess(target)
    _, train_ids, pred_lens = ml.get_train_test(ml.STEP)
    train_ids = [train_ids[x] for x in np.random.permutation(len(train_ids)).tolist()]  # Shuffling
    # Selecting wells without failure
    good_wells = select_good_wells(train_ids, n_well)
    params = opt_variance_zero(good_wells, pred_lens, target)
    return params

def ffr_sub(inp, target, test_ids, train_ids, pred_lens):
    """
    Runs FFRs for inp and returns the accuracy.
    """
    sigma = inp[-1]
    dim = int((inp.shape[0]-1)/2)
    temp = abs(inp[:dim])/np.sum(abs(inp[:dim]))
    params = [temp.tolist(), inp[dim:2*dim].tolist()]
    yhat, y_test, _ = ml.fitc_predictor(target, test_ids, train_ids, pred_lens, params, sigma, 300)
    return ml.evaluate_error(y_test, yhat, measure='median')

def opt_predictive_zero(ids, pred_lens, target, N=10, fdim=6):
    """
    Optimizing the kernel parameters based on prediction error and zero-order optimization.
    """
    dim = fdim*2+1
    params, results = list(), list()
    for _ in range(N):
        init = np.array([np.exp(np.random.uniform(-3, 3)) for _ in range(dim)])
        arg = (target, ids[:1000], ids[1000:5000], pred_lens)
        opt_res = minimize(ffr_sub, init, arg, method='Nelder-Mead', options={'maxiter':50})
        params.append(opt_res['x'])
        results.append(opt_res['fun'])

    ind = np.argsort(results)[:1]
    params = np.mean(np.array(params)[ind, :], axis=0)
    params[:fdim] = abs(params[:fdim])/np.sum(abs(params[:fdim]))
    # Saving to db
    print ', '.join('{0:.3f}'.format(x) for x in params)
    hpf.savePars2db("bigprod_ts", ml.FEATURES.keys(), target, params, ml.WINP, ml.STATE)
    return params

def opt_var_zero_2_only(inp, target, params, ind, pred_lens):
    """
    Sub-function for being called by the optimization module.
    """
    fdim = int((len(params)-1)/2)
    params[:fdim] = params[:fdim]*inp[0]
    params[-1] = params[-1]*inp[1]
    return neg_log_likelihood(params, ind, pred_lens, target)

def opt_variance_zero_two_only(target, N=2):
    """
    This one uses maximum likelihood to tune the scaling factor of the kernel.
    """
    np.random.seed(0)   # For reproducability
    ml.load_data(target)
    ml.preprocess(target)
    _, train_ids, pred_lens = ml.get_train_test(ml.STEP)
    train_ids = [train_ids[x] for x in np.random.permutation(len(train_ids)).tolist()]  # Shuffling

    params = np.array([5.02493592, 0.30290206, 6.19230297, 2.20992589,
                       2.11517852, 4.21499677, 0.07009884, 1.44529921,
                       3.20938043, 17.86920671, 5.51474997, 0.22895883, 6.52829307])
    fdim = 6
    params[:fdim] = abs(params[:fdim])/np.sum(abs(params[:fdim]))
    pars, results = list(), list()
    for _ in range(N):
        init = np.exp(np.random.uniform(-3, 3, 2))
        arg = (target, params, train_ids[:5000], pred_lens)
        opt_res = minimize(opt_var_zero_2_only, init, arg, method='Nelder-Mead', options={'maxiter':2})
        pars.append(opt_res['x'])
        results.append(opt_res['fun'])
    return opt_res['x'], opt_res['fun']

def opt_kernel_pars_pred(target, N, fdim, source, features, length, state):
    """
    Main function for optimization based on prediction.
    """
    np.random.seed(0)   # For reproducability
    ml.load_data(target)
    ml.preprocess(target)
    _, train_ids, pred_lens = ml.get_train_test(ml.STEP)
    train_ids = [train_ids[x] for x in np.random.permutation(len(train_ids)).tolist()]  # Shuffling
    # Selecting wells without failure
    params = opt_predictive_zero(train_ids, pred_lens, target, N, fdim)
    hpf.savePars2db(source, features, target, params, length, state)
    return params

def opt_nn_sub(params, test_its, setting):
    dim = int((len(params)-3)/2)
    setting['params'] = [params[0:dim], params[dim:2*dim]]
    setting['sigma'] = params[-3:]
    yhat, y_test, _ = nn.run_ffr(test_its, setting)
    yhat, y_test = np.vstack(tuple(yhat)).astype(float), np.vstack(tuple(y_test)).astype(float)
    return nn.evaluate_error(y_test, yhat, 'median')

def opt_nn_pred(N=3):
    setting = {'target':'oil', 'state':'RAND', 'table_name':'test_RAND', 'params':None,
               'test_date':nn.TEST_DATE, 'n_train':100, 'winp':nn.WINP, 'winq':nn.WINQ, 'limit':500}
    np.random.seed(0)   # For reproducability
    log = True
    nn.load_data(setting['target'], setting['state'])
    nn.preprocess(setting['target'])
    test_it = nn.select_test_wells(setting, test_date=nn.TEST_DATE, winq=nn.WINQ)

    pars, results = list(), list()
    for _ in range(N):
        init = np.exp(np.random.uniform(-3, 3, 13))
        arg = (test_it, setting)
        opt_res = minimize(opt_nn_sub, init, arg, method='Nelder-Mead', options={'maxiter':2})
        pars.append(opt_res['x'])
        results.append(opt_res['fun'])
    return results, pars

def search_nn_pred(N=50, n_test=1000):
    setting = {'target':'oil', 'state':'RAND', 'table_name':'test_RAND', 'params':None,
               'test_date':nn.TEST_DATE, 'n_train':100, 'winp':nn.WINP, 'winq':nn.WINQ, 'limit':500}
    np.random.seed(0)   # For reproducability
    log = True
    nn.load_data(setting['target'], setting['state'])
    nn.preprocess(setting['target'], True)
    test_it = nn.select_test_wells(setting, test_date=nn.TEST_DATE, winq=nn.WINQ)[:n_test]

    res_file = open('opt_results.pkl', 'wb')
    log_file = open('log_file.txt', 'wt')
    pars, results = list(), list()
    for _ in range(N):
        params1 = np.exp(np.random.uniform(-2, 0, 6))
        params1 = params1/sum(params1)
        params2 = np.exp(np.random.uniform(-1, 2, 6))
        sigma = np.concatenate( (np.exp(np.random.uniform(-1, 2, 2)), np.random.uniform(1e-2, 1, 1)) )
        params = np.concatenate((params1, params2, sigma))
        err = opt_nn_sub(params, test_it, setting)
        pars.append(params)
        results.append(err)
        log_file.write('{0:0.4f}\n'.format(err))
        log_file.flush()
        pk.dump({'pars':pars, 'result':results}, res_file)
    res_file.close()
    log_file.close()
    return results, pars

if __name__ == "__main__":
    # import pdb;
    # pdb.set_trace()
    results, pars = search_nn_pred(N=30)
    import pdb; pdb.set_trace()
    print pars[np.argmin(np.array(results))]
    # print opt_kernel_pars_pred('wor', N=10, fdim=7)
