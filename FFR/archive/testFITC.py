import numpy as np
from collections import defaultdict
from scipy.spatial.distance import cdist
from numpy.linalg import inv, norm


def genIndexes(n, testRatio=0.3):
    np.random.seed(0)   # For reproducability
    indexes = np.random.permutation(n)
    # 20% Test
    nTest = int(testRatio*n)
    indTest = indexes[0:nTest]
    indTrain = indexes[nTest:]
    return np.array(indTest),np.array(indTrain)

def getData(n):
    data = dict()
    data['X'] = np.random.normal(0, 1, (n,3))
    data['y'] = ((data['X'][:,0] > 0) *1.0)[:,np.newaxis]
    return data

def selectInducingPoints(data, ind, nInduce=300):
    inds = np.random.permutation(len(ind))
    inducingPoints = dict()
    inducingPoints['X'] = data['X'][ind[inds[:nInduce]],:]
    inducingPoints['y'] = data['y'][ind[inds[:nInduce]],:]
    return inducingPoints

def kernelInducingPoints(data, ind, inducingPoints):
    import pdb; pdb.set_trace()
    KO = cdist(inducingPoints['X'], data['X'][ind,:]);   
    return np.exp(-KO) 

def kernelDiagonal():
    return 1

def fDP(dt, mat):  # fastDiagProduct
    return np.tile(dt[:,np.newaxis], (1, mat.shape[1]))*mat

def getKernelUU(inducingPoints):
    n = inducingPoints['X'].shape[0]
    Kuu = np.zeros((n,n))
    for i in range(n):
        Kuu[:,i] = kernelInducingPoints(inducingPoints, i, inducingPoints)[:,0]
    return Kuu

def getKernelXU(data, inds, inducingPoints):
    # Returns a nX x nU matrix, an nX x T matrix, and an array of size nX
    nX,nU = len(inds),inducingPoints['X'].shape[0]
    Kx = np.zeros((nX,nU))
    for (i,jn) in enumerate(inds):
        Kx[:,i] = kernelInducingPoints(data, jn, inducingPoints)[:,0]
    return Kx, data['y'][inds], np.ones((nX,1))

def fitcPredictor(data, indTrain, indTest):
    import pdb; pdb.set_trace()
    sigma = 0.1
    inducingPoints = selectInducingPoints(data, indTrain, nInduce=300)
    Kuu = getKernelUU(inducingPoints)
    Kuui = inv(Kuu)
    Kxu, yTrain, dg = getKernelXU(data, indTrain, inducingPoints)
    Kzu, yTest, _ = getKernelXU(data, indTest, inducingPoints)
    dti = 1/(dg - np.sum(Kxu*(np.dot(Kuui,Kxu.T)).T,axis=1) + (sigma**2)*np.ones((dg.shape[0],) )) 

    yt = fDP(dti, yTrain)
    ytt =  np.dot(fDP(dti,Kxu), np.dot(inv(Kuu + np.dot(Kxu.T,fDP(dti, Kxu))), np.dot(Kxu.T, yt) ))
    yhat = np.dot(Kzu, np.dot(Kuui, np.dot(Kxu.T, yt-ytt)))
    return yhat, yTest

if __name__ == "__main__":
    n = 1000
    data = getData(n)
    indTest,indTrain = genIndexes(n, testRatio=0.3)
    # 
    yhat, yTest = fitcPredictor(data, indTrain, indTest)
    print norm(yhat-yTest, ord='fro')/norm(yTest, ord='fro')




