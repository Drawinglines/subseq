from __future__ import division
import cPickle as pk
import numpy as np
from numpy.linalg import inv, norm
from datetime import datetime, date
import core_ml_ffr as ml 

def selectGoodWells(trainIDs, N=1000):
	ind, cntr = list(), 0
	for ii in trainIDs:
		eqZ = [x==0 for x in ml.data['oil'][ii[0]][ii[1]:ii[1]+ml.winp+ml.winq]]
		if sum(eqZ) == 0:
			ind.append(ii)
			cntr +=1
		if cntr > N:
			break
	return ind

def negLogLikelihood(params, ind, predLens):
    """
    This function computes the negative log-likelihood of 1000 training points given the parameters.  
    The goal is to find the best hyper-parameters by searching over them.
    """
    sigma = params[-1]
    try:
        Kxx, y, _, _, _, _ = getKernelXU(ind, ind, ml.lens, predLens, params, test=False)
        n = Kxx.shape[0]
        (sgn, logdet) = np.linalg.slogdet(Kxx+(sigma**2)*np.eye(n))
        return np.trace(np.dot( y.T, np.dot( inv((sigma**2)*np.eye(n)+Kxx), y )))/winq + logdet
    except:
        return np.inf 

def optVariance(trainIDs, predLens, parm, N=50): # 50
    """ 
    Randomized search for the best hyperparameters using the function negLogLikelihood().
    This function is independent of the function ffr() and it should be run separately.
    """
    params, results = list(), list()
    for i in range(N):
        coef, var = [np.exp(np.random.uniform(-3, 3)) for j in range(2)]
        par = [parm[0]*coef, parm[1]*coef, parm[2]*coef, parm[3], parm[4], parm[5], var]
        res = negLogLikelihood(par, trainIDs, predLens)
        params.append(par)
        results.append(res)
    ind = np.argsort(results)[:5]
    return np.mean(np.array(params)[ind,:], axis=0)

def mainPredictor(trainIDs, predLens, params):
    try:
        yOhat, yOTest, _, _, _ = ml.fitcPredictor(trainIDs[500:1000], trainIDs[:500], ml.lens, predLens, params)
        return norm((yOhat-yOTest)*(yOTest>0), ord='fro')/norm(yOTest, ord='fro')
    except:
        return np.inf 

def optMean(trainIDs, predLens, N=500): # 500 
    """ 
    Randomized search for the best hyperparameters using the function negLogLikelihood().
    This function is independent of the function ffr() and it should be run separately.
    """
    params, results = list(), list()
    for i in range(N):
        par = [np.exp(np.random.uniform(-3, 3)) for j in range(7)]
        for j in range(3):
        	par[j]/= sum(par[:3])	# Some normalization
        res = mainPredictor(trainIDs, predLens, par)
        params.append(par)
        results.append(res)
    ind = np.argsort(results)[:10]
    return np.mean(np.array(params)[ind,:], axis=0)

if __name__ == "__main__":
    np.random.seed(0)   # For reproducability
    ml.loadData()
    ml.preprocess()
    testIDs, trainIDs, predLens = ml.getTrainTest(ml.step)
    trainIDs = [trainIDs[x] for x in np.random.permutation(len(trainIDs)).tolist()]  # Shuffling 
    params = optMean(trainIDs, predLens)
    print params
    goodWells = selectGoodWells(trainIDs, 1000)
    params = optVariance(goodWells, predLens, params)
    print params
