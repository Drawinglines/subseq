import cPickle as pk
from collections import defaultdict
import pdb

data_all = pk.load(open('dataRAND.pkl', 'rb'))

print len(data_all['padID'])

new_len = 1000

data = defaultdict(list)
data['padID'] = data_all['padID'][:new_len]
data['dates'] = data_all['dates'][:new_len]
data['oil'] = data_all['oil'][:new_len]
data['water'] = data_all['water'][:new_len]
data['proddays'] = data_all['proddays'][:new_len]
data['location'] = data_all['location'][:new_len]
data['depth'] = data_all['depth'][:new_len]
data['pool'] = data_all['pool'][:new_len]

pdb.set_trace()
pk.dump(data, open('data1k.pkl', 'wb'))
