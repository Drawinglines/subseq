"""  
This file implements the core machine learning part of the FFR algorithm.  
The db interface and auxilliary functions are in 'helpers_ffr' module.

The main function in this file is `ffr()`.

Note that the FFR algorithm cannot be understood without the mathematical
 description provided in the following link:
http://terra.ai:8090/display/forteam/FFR+and+SSM

Parameters and settings:
# state:    the state to be analyzed.
# winp:     length of the sequence used for training
# winq:     max length of the testing period
# nInduce:  number of inducing points in FITC
# sm:       number of time steps used for linear regression in 
#           finding the bias term.
# testDate: start date for testing period
"""

from __future__ import division
import cPickle as pk
import numpy as np
from collections import defaultdict
from scipy.spatial.distance import cdist
from numpy.linalg import inv, norm
from sklearn.linear_model import Ridge
from datetime import datetime, date
import helpers_ffr as hpf
from tqdm import tqdm   # For progress bar
import warnings
warnings.filterwarnings("ignore")  # Not a very brilliant idea!

state = 'NDK'
winp = 36
winq = 84
step = 6
nInduce = 2000
LOG = False         # Should we be in Logarithmic domain or linear
sm = 5              # No. of last points used for detrending
cndLim = nInduce     # Condition number of the kernel for the inducing parameters.
clf = Ridge(alpha=0.1)
testDate = hpf.diff_month(datetime.strptime('2009-01-01' , '%Y-%m-%d'))

def loadData():
    """ Globally Loading the Data.
    It indends to improve efficiency of the algorithms that use the large `data` variable.
    """
    global data, lens
    data = hpf.readFromDB(stateID=[hpf.stateTable[state]])
    lens = hpf.getLength(data['oil'])

def preprocess():
    """ This function implements the most basic preprocessing for the data.
    It does the followings:
    # Makes sure that all of the values are finite.
    # Converts monthly oil and water to daily via dividing them by `proddays`.
    # Takes the log of oil and water.

    NOTE: This function is the only function that changes the global variable `data` after 'loadData()'.
    """
    for i in range(len(data['oil'])):
        # NaNs
        data['proddays'][i][np.isnan(data['proddays'][i])] = 0
        data['oil'][i] /= data['proddays'][i]                   # This makes oil per day (oil_pd)
        data['water'][i] /= data['proddays'][i]                   # This makes water per day (water_pd)
        # Take log of oil
        data['oil'][i] = np.log10(1+data['oil'][i])         # log(1+x) to make sure that it is invertable
        data['oil'][i][~np.isfinite(data['oil'][i])] = 0 
        data['water'][i] = np.log10(1+data['water'][i])         # log(1+x) to make sure that it is invertable
        data['water'][i][~np.isfinite(data['water'][i])] = 0

def getTrainTest(step):
    """ This function produces two lists.  
    Each one of them list of tuples (i, t), where i is the well index and t is the time of testing.

    The first list is the list of all indexes for the test wells plus the testing time for them.
    The second one is the list of training (i, t).
    This function ensures that the testing criteria given in the confluence page are met.
    """
    testIDs, trainIDs, predLens = list(), list(), list()
    for i in range(len(data['oil'])):
        if (testDate-winp in data['dates'][i].tolist()) and (testDate in data['dates'][i].tolist()):
            t = testDate-winp-min(data['dates'][i])
            predLen = min(lens[i],t+winq+winp) - (t+winp)       # How long we can predict
            if predLen > 0:
                testIDs.append( (i,t) )
                predLens.append( predLen )
        else:
            for t in range(min(data['dates'][i]), min(max(data['dates'][i]), testDate-winp-winq), step):
                if t+winp+winq in data['dates'][i].tolist():
                    trainIDs.append( (i,t-min(data['dates'][i]))     )
                else: 
                    break
    return testIDs, trainIDs, predLens

def checkMatrix(Kuu, inducingPoints, jn, params):
    """
    This function checks if adding the new inducing point identified by 'jn' to the 
    set of 'inducingPoints' will guarantee that the condition number of their joint
    kernel matrix is less than 'cndLim'.  This is aimed at making FFR with FITC more 
    stable.
    """
    if Kuu.size == 0:
        return np.array([[kernelDiagonal(params)]]), True
    Kup = kernelInducingPoints(jn, inducingPoints, params)
    KuuNew = np.hstack((Kuu,Kup))
    KuuNew = np.vstack((KuuNew, np.hstack( (Kup.T,np.array([[Kuu[0,0]]]) ))))
    passed = ( np.linalg.cond(KuuNew) < cndLim )
    if passed:
        return KuuNew, passed
    else:
        return Kuu, passed

def selectInducingPoints(inds, params, nInduce=nInduce):
    """
    FITC needs a set of inducing points. Selecting those points randomly usually 
    creates numerical stability problems.  This function implements a simple loop 
    that selects a point if its addition to the set of currently selected points
    preserves the low condition number of the kernel matrix.

    The inducing points are selected from the training data indexes specified in 
    'ind'.
    """
    inds = [inds[x] for x in np.random.permutation(len(inds)).tolist()]  # Shuffling for this purpose
    inducingPoints = list()
    counter, i, Kuu = 0, 0, np.array([])
    print "Finding the inducing points..."
    for i in tqdm(xrange(len(inds))):
        Kuu, passing = checkMatrix(Kuu, inducingPoints, inds[i], params)
        if passing:
            inducingPoints.append(inds[i])
            counter += 1
        if counter == nInduce:  break
    return inducingPoints, Kuu

def kernelInducingPoints(jn, inducingPoints, params):  
    """ 
    Computes the kernel between a new point 'jn' and the 'inducingPoints'.  
    The result is a vertical vector of size n by 1 where n = |inducingPoints|.
    """
    cO, cW, cD, sO, sW, sD,_ = params
    KO = [cdist(data['oil'][x[0]][x[1]:x[1]+winp][None,:], data['oil'][jn[0]][jn[1]:jn[1]+winp][None,:]) for x in inducingPoints]  
    KW = [cdist(data['water'][x[0]][x[1]:x[1]+winp][None,:], data['water'][jn[0]][jn[1]:jn[1]+winp][None,:]) for x in inducingPoints]  
    KD = [cdist(data['proddays'][x[0]][x[1]:x[1]+winp][None,:], data['proddays'][jn[0]][jn[1]:jn[1]+winp][None,:]) for x in inducingPoints]  
    return cO*np.exp(-sO*np.vstack(tuple(KO))) + cW*np.exp(-sW*np.vstack(tuple(KW))) + cD*np.exp(-sD* np.vstack(tuple(KD)))

def kernelDiagonal(params):
    """
    Get the maximum value of the kernel.
    """
    cO, cW, cD, sO, sW, sD,_ = params
    return cO + cW + cD

def fDP(dt, mat):  # fastDiagProduct
    """
    A simple function to perform matrix multiplication when the first matrix 'dt' is diagonal.
    """
    return np.tile(dt[:,np.newaxis], (1, mat.shape[1]))*mat

def getXLast(ys):
    """
    The goal of this function is to perfrom a linear regression and estimate
    the next value of the time series. 
    It uses linear regression on the last 'sm' points of the time series 'ys'.

    # ys is an list of n x 1
    """
    n = len(ys)
    X, y = list(), list()
    for i in range(n-sm-1):
        X.append(ys[i:i+sm])
        y.append(ys[i+sm])
    clf.fit(np.array(X), np.array(y)[:,np.newaxis])
    return clf.predict(np.array(X[-1]))[0,0]
    # return ys[-1]

def getKernelXU(inds, inducingPoints, lens, predLens, params, test=False):
    """  
    This function build the kernels between the training and testing data points and 
    the inducing points 'Kx'.  It also extracts the qt time series (here called y) 
    time series.  It does this for both oil (yO) and water (yW). It also outputs the 
    best linear estimator for the next month oil and water production using `getXLast()` 
    function. Finally, it calculates the diagonal elements of the kernel matrix for the 
    input data using the `kernelDiagonal()` function.

    Returns a nX x nU matrix, an nX x T matrix, and an array of size nX
    """
    Kx, yO, yW, diag, xOlast, xWlast = [list() for i in range(6)]
    print "Constructing {} kernels:".format("test" if test else "training")
    for (i, jn) in enumerate(tqdm(inds)):
        Kx.append( kernelInducingPoints(jn, inducingPoints, params)[:,0] )
        diag.append(kernelDiagonal(params))
        if np.isnan(Kx[-1]).sum()>0:    pdb.set_trace()

        # Processing the output time series
        xO = getXLast( hpf.deprocess( data['oil'][jn[0]][jn[1]:jn[1]+winp], LOG ) )
        xOlast.append( xO )
        xW = getXLast( hpf.deprocess( data['water'][jn[0]][jn[1]:jn[1]+winp], LOG ) )
        xWlast.append( xW )
        if test:
            # Get the oil one
            ytemp = np.zeros((winq,))
            ytemp[:predLens[i]] = hpf.deprocess( data['oil'][jn[0]][jn[1]+winp:jn[1]+winp+predLens[i]], LOG )
            yO.append(ytemp)
            # Now the water one
            ytemp = np.zeros((winq,))
            ytemp[:predLens[i]] = hpf.deprocess( data['water'][jn[0]][jn[1]+winp:jn[1]+winp+predLens[i]], LOG )
            yW.append(ytemp)
        else:
            # For oil
            yO.append( np.array( hpf.deprocess(data['oil'][jn[0]][jn[1]+winp:jn[1]+winp+winq], LOG)) - xO )
            # For water
            yW.append( np.array( hpf.deprocess(data['water'][jn[0]][jn[1]+winp:jn[1]+winp+winq], LOG) ) - xW )

    return np.array(Kx), np.array(yO), np.array(yW), np.array(diag), np.array(xOlast)[:,None], np.array(xWlast)[:,None]

# def postProcess(yhat):
#     slope = np.mean(yhat-yhat[:,0][:,np.newaxis], axis=1, keepdims=True)*2/(win-1)
#     slope = slope * (slope > 0)
#     return yhat - np.dot(slope, np.arange(win)[np.newaxis,:])

def fitcPredictor(testIDs, trainIDs, lens, predLens, params):
    """
    This function is the implemetation of the core FITC algorithm. It performs the following steps:
    # Selects a set of inducing points.
    # Calculates the training and testing kernels with the inducing points.
    # Runs FITC approximation for (1) oil, (2) water, and obtains the predictive variance.

    In this function, I have closely followed the description in the following paper:

        Chalupka, K., Williams, C. K., & Murray, I. (2013). 
        A framework for evaluating approximation methods for Gaussian process regression. 
        The Journal of Machine Learning Research, 14(1), 333-350.

    It is strongly recommended to read it before trying to understand the math here.
    """
    sigma = params[-1]
    inducingPoints, Kuu = selectInducingPoints(trainIDs, params, nInduce=nInduce)
    print "Got the inducing points"
    Kuui = inv(Kuu)
    Kxu, yOTrain, yWTrain, dg, _, _ = getKernelXU(trainIDs, inducingPoints, lens, predLens, params, test=False)
    Kzu, yOTest, yWTest, _, xOlast, xWlast = getKernelXU(testIDs, inducingPoints, lens, predLens, params, test=True)
    dti = 1/(dg - np.sum(Kxu*(np.dot(Kuui,Kxu.T)).T,axis=1) + (sigma**2)*np.ones((dg.shape[0],) )) 
    oScale = np.max(abs(yOTrain))  # This is meant for numerical stability
    wScale = np.max(abs(yWTrain))
    
    print "Matrix multiplications..."
    # Oil
    yt = fDP(dti, yOTrain/oScale)
    ytt =  np.dot(fDP(dti,Kxu), np.dot(inv(Kuu + np.dot(Kxu.T,fDP(dti, Kxu))), np.dot(Kxu.T, yt) ))
    yOhat = np.dot(Kzu, np.dot(Kuui, np.dot(Kxu.T, yt-ytt)))
    # Water
    yt = fDP(dti, yWTrain/wScale)
    ytt =  np.dot(fDP(dti,Kxu), np.dot(inv(Kuu + np.dot(Kxu.T,fDP(dti, Kxu))), np.dot(Kxu.T, yt) ))
    yWhat = np.dot(Kzu, np.dot(Kuui, np.dot(Kxu.T, yt-ytt)))
    # Variance
    yt = fDP(dti, np.dot(Kzu, np.dot(Kuui, Kxu.T)).T)
    ytt =  np.dot(fDP(dti,Kxu), np.dot(inv(Kuu + np.dot(Kxu.T,fDP(dti, Kxu))), np.dot(Kxu.T, yt) ))
    kzz = np.array([ kernelInducingPoints(x, [x], params)[0,0] for x in testIDs])
    vhat = kzz - np.diag(np.dot(Kzu, np.dot(Kuui, np.dot(Kxu.T, yt-ytt))))
    shat = np.sqrt(vhat)

    return yOhat*oScale+xOlast, yOTest, yWhat*wScale+xWlast, yWTest, shat

def ffr():
    """
    This is the top function that calls functions in the right sequence and prints the proper output 
    in the screen.  Please follow the print functions' output to see this function's objective.
    """
    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Starting FFR on state {}.".format(state))
    np.random.seed(0)   # For reproducability
    loadData()
    print "Pre-processing the data."
    preprocess()
    testIDs, trainIDs, predLens = getTrainTest(step)
    print "Got the indexes"
    params = [0.25, 0.25, 0.23, 1.27, 3.74, 4.6, 0.05]   # Last parameter can be higher
    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Running the FFR with GP/FITC algorithm.") 
    yOhat, yOTest, yWhat, yWTest, shat = fitcPredictor(testIDs, trainIDs, lens, predLens, params)
    print "Noramlized RMSE for oil forecasting:\t" + str(norm((yOhat-yOTest)*(yOTest>0), ord='fro')/norm(yOTest, ord='fro'))
    print "Noramlized RMSE for water forecasting:\t" + str(norm((yWhat-yWTest)*(yWTest>0), ord='fro')/norm(yWTest, ord='fro'))
    print "Postprocessing the data..."
    dataOut = hpf.convertData(data, yOhat, yWhat, shat, testIDs, predLens, winp, LOG)
    print "Writing into the database..."
    tableName = 'FFR_{}_{}_{}'.format(state, nInduce, winp)
    hpf.write2db(dataOut, tableName)
    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Finished FFR on state {}.".format(state))
    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Forecasts are in table {}.".format(tableName))

if __name__ == "__main__":
    ffr()
