"""
This module includes a set of helper functions for FFR:
# Reading from database
# Pre-processing the data
# Post-processing the data
# Writing into database

Parameters and settings:
# initDate      : A reference date for converting dates to month differences
# indexDate     : start date for testing period
# stateTable    : Dictionary of state names and IDs.
"""
from __future__ import division
from collections import defaultdict
import MySQLdb
import numpy as np
from datetime import datetime, date
from calendar import monthrange
import cPickle as pk
from tqdm import tqdm   # For progress bar

initDate = datetime.strptime('1900-01-01' , '%Y-%m-%d')
indexDate = datetime.strptime('2008-01-01' , '%Y-%m-%d')
stateTable = {'CA':4, 'CO':5, 'NM':30, 'NDK':33, 'OK':35, 'UT':43, 'AK':50, 'WY':49}

def diff_month(d1):
    """
    Calculates the difference (d1-initDate) in terms of number of months in between them.
    """
    try:
        return (d1.year - initDate.year)*12 + d1.month - initDate.month
    except:
        import pdb; pdb.set_trace()
        print "Exception.  The aregument was {}.".format(d1)
        return np.NaN

def add_months(months):
    """
    Returns the date for several months after the initDate. 
    """
    month = initDate.month - 1 + months
    year = int(initDate.year + month / 12 )
    month = month % 12 + 1
    return date(year,month,monthrange(year,month)[1])  # Reporting on the last day of the month

def getLength(oil):
    """
    Calculates the length of oil time series.  
    Here, the last data point of the time series is the last non-zero production month.
    """
    N = len(oil)
    lens = [x.shape[0] for x in oil]
    for i in range(N):
        while oil[i][lens[i]-1] == 0:
            lens[i] -= 1
            if lens[i] == 0: break
    return lens

def mynum(func, a):
    ''' This function is only a way to get around the unexpected data while converting it to type specified by 'func' argument.
    You may want to write a more sophisticated function for treatment of bad entries.'''
    try:
        return func(a)
    except:
        print('Expected float, got {} instead'.format(type(a)))
        return 0

def handleMissing(dates, oil, water, days):
    """
    This function handles the missing data points by inserting zeros in the missing months.
    """
    new_dates = np.arange(min(dates), max(dates)+1) 
    new_oil, new_water, new_days = [np.zeros(new_dates.shape) for _ in range(3)]
    new_oil[dates-min(dates)] = oil
    new_water[dates-min(dates)] = water
    new_days[dates-min(dates)] = days
    return new_dates, new_oil, new_water, new_days

def readFromDB(stateID=[33]):
    """
    This function reads the data from the database and converts them to a defaultdict format.  
    It also performs some mild preprocessing on the data such as convertion of the dates to diff months and 
    handling missing data with handleMissing() function.
    """
    # Establish the connection
    cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com','user':'taha','password': 'taha@terraai','db': 'public_data'}
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()

    query = "SELECT API_prod,dates,oil,water,proddays FROM bigprod_ts WHERE state in ({});".format(','.join([str(x) for x in stateID]))
    cursor.execute(query)
    row = cursor.fetchall()
    data = defaultdict(list)
    # 
    print "Reading from the database:"
    for item in tqdm(row):
        # Exclude all-zero rows
        oil = np.array([mynum(float,x) for x in item[2].split(',')])
        if oil.sum() == 0:  continue
        
        dates  = np.array([diff_month(datetime.strptime(x, '%Y-%m-%d')) for x in  item[1].split(',')])
        index = np.argsort(dates)
        dates = dates[index]
        water = np.array([mynum(float,x) for x in item[3].split(',')])[index]
        days = np.array([mynum(int,x) for x in item[4].split(',')])[index]
        dates, oil, water, days = handleMissing(dates, oil[index], water, days)

        data['padID'].append( item[0] )
        data['dates'].append( dates )
        data['oil'].append( oil )
        data['water'].append( water )
        data['proddays'].append( days )
        # data['year'].append( dates.min() )
        # data['latitude'].append( item[6] )
        # data['longitude'].append( item[7] )
        # query = "SELECT API_prod,dates,oil,water,proddays,spud_date,lat,longi FROM bigprod_ts WHERE state in ({});".format(','.join([str(x) for x in stateID]))

    cursor.close()
    conn.close()
    return data

def deprocess(x, log=False, mode=True):
    """
    Removes the preprocessing from the data.
    """
    if mode:    # This is the default mode to return the original one
        return x if log else np.power(10, x)-1 
    else:       # When post post processing
        return np.power(10, x)-1 if log else x

    
def formatForecast(shape, hat, ln, winp, jn, LOG):
    """
    This is a post-processing function. 
    # It removes the part of forecast that is beyond the length of the actual time series.
    # It reverses the log operation in the preprocessing step.
    # It ensures that the output is non-negative.
    """
    fore = np.nan*np.empty(shape)
    temp = deprocess( hat[:ln], LOG, False ) 
    fore[jn[1]+winp:jn[1]+winp+ln] = (temp*(temp>=0))
    return fore

def confidence(yOhat, yWhat, shat):
    """
    Assuming Gaussian noise, we use the z-scores to find 0.1 and 0.9 percentiles of the forecasts.
    """
    z = 1.645
    oil10 = yOhat - z*shat[:, None] 
    oil90 = yOhat + z*shat[:, None] 
    water10 = yWhat - z*shat[:, None] 
    water90 = yWhat + z*shat[:, None] 
    return oil10, oil90, water10, water90

def convertData(dataIn, yOhat, yWhat, shat, ids, predLen, winp, LOG):
    """ 
    This function converts the data to the right defaultdict format for writing into the db. 
    It reverses the preprocessing operations and uses formatForecast() to cut the time series beyond the length of actual time series.
    Also, it invokes confidence() to calculate the percentile curves.
    """
    data = defaultdict(list)
    for (i, jn) in enumerate(ids):
        data['padID'].append(dataIn['padID'][jn[0]])
        data['date'].append( np.array( [add_months(x) for x in dataIn['dates'][jn[0]].tolist()] ) )
        data['oil'].append( deprocess( dataIn['oil'][jn[0]], LOG, False) )
        data['proddays'].append(dataIn['proddays'][jn[0]])
        data['water'].append( deprocess( dataIn['water'][jn[0]], LOG, False) ) 

        shape = dataIn['water'][jn[0]].shape
        oil10, oil90, water10, water90 = confidence(yOhat, yWhat, shat)
        ss = (np.log10(1+shat[i]) if LOG else shat[i])*np.ones(yWhat[i,:].shape)  # This is a hack to be able to reuse the existing function
        data['water_fore'].append( formatForecast(shape, yWhat[i,:], predLen[i], winp, jn, LOG) ) 
        data['water_fore_std'].append( formatForecast(shape, ss, predLen[i], winp, jn, LOG) ) 
        data['water_fore10'].append( formatForecast(shape, water10[i,:], predLen[i], winp, jn, LOG) ) 
        data['water_fore90'].append( formatForecast(shape, water90[i,:], predLen[i], winp, jn, LOG) ) 
        data['oil_fore'].append( formatForecast(shape, yOhat[i,:], predLen[i], winp, jn, LOG) ) 
        data['oil_fore_std'].append( formatForecast(shape, ss, predLen[i], winp, jn, LOG) ) 
        data['oil_fore10'].append( formatForecast(shape, oil10[i,:], predLen[i], winp, jn, LOG) ) 
        data['oil_fore90'].append( formatForecast(shape, oil90[i,:], predLen[i], winp, jn, LOG) ) 
    return data

def write2db(data, name):
    """
    This function writes the data formatted and returned by convertData() into the database. 
    """
    # Establish the connection
    cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com',
            'user':'taha','password': 'taha@terraai','db': 'results'}    
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()
    
    # Drop table if exists
    query = "DROP TABLE IF EXISTS {};".format(name)
    cursor.execute(query)

    query = """CREATE TABLE {} (API_prod BIGINT, report_date_mo DATE, proddays INT, 
    oil_pd DOUBLE, oil_pd_fore_mean DOUBLE, oil_pd_fore_std DOUBLE, oil_pd_fore_p10 DOUBLE, oil_pd_fore_p90 DOUBLE, 
    wtr_pd DOUBLE, wtr_pd_fore_mean DOUBLE, wtr_pd_fore_std DOUBLE, wtr_pd_fore_p10 DOUBLE, wtr_pd_fore_p90 DOUBLE);""".format(name)
    cursor.execute(query)

    # Insert the rows
    values = list()
    for (i, well) in enumerate(data['padID']):
        for j in range(len(data['oil'][i])):
            values.append( "('{}','{}',{},{},{},{},{},{},{},{},{},{},{})".format(well, data['date'][i][j],data['proddays'][i][j], \
             data['oil'][i][j], data['oil_fore'][i][j], data['oil_fore_std'][i][j], data['oil_fore10'][i][j], data['oil_fore90'][i][j], \
             data['water'][i][j], data['water_fore'][i][j], data['water_fore_std'][i][j], data['water_fore10'][i][j], data['water_fore90'][i][j]))

    stepSize = 1000    # Doing the queries in batches of size 1000
    nBatch = int(np.ceil(len(values)/stepSize))
    ind = [min(x*stepSize,len(values)) for x in range(nBatch)]
    for i in tqdm(range(nBatch-1)):
        query = 'INSERT INTO {} VALUES {};'.format(name,','.join(values[ind[i]:ind[i+1]]))
        query = query.replace('nan', 'NULL')
        query = query.replace('inf', 'NULL')
        cursor.execute(query)

    conn.commit()
    cursor.close()
    conn.close()

if __name__ == "__main__":
    # pdb.set_trace()
    data = readFromDB()


