"""
This is the top file for calling FFR.
See the documentation of 'main_ffr()' for how to use it.
"""

from __future__ import division

import helpers_ffr as hpf
import core_ml_ffr as ml
import opt_ffr as opt

def main_ffr(target, state, source, features, length, table_name):
    """
    This function is the main interface to FFR and how we should call it.

    It automatically checks to see if the kernel parameters are available in the database
    for the given settings. If note available, it calls the corresponding function from the
    optimization module and gets the parameters. This process will take some time.

    Args:
      target: A string that specifies the quantity that we want to forecast; e.g.: 'oil'.
      state: A string that specifies the state for analysis; e.g.: 'NDK'
      source: A string that specifies the source table in the database; e.g.: 'bigprod_ts'
      features: A list of features that are available.
        E.g.: ['oil', 'water', 'proddays', 'location', 'depth', 'pool']
      length: An integer that specifies the length of historical data in months
        to be used. E.g.: 36
      table_name: The name of output table in the database.

    Returns:
      Nothing.  It writes the forecasts into 'table_name'.
    """
    # Check if the params are available
    params = hpf.checkKernelParindb(source, features, target, length, state)
    if params is None:
        # Call opt to get params
        print "Need to get the kernel parameters."
        dim = len(features) if target in features else len(features)+1
        params = opt.opt_kernel_pars_pred(target, 10, dim, source, features, length, state)
    ml.ffr(target, state, table_name, params)

def run_all():
    """
    This function runs the algorithm in a set of states and target variables.
    """
    # states = ['WY', 'NM', 'OK', 'UT', 'AK', 'CO']
    states = ['NDK', 'NM', 'WY']
    # features = ['oil', 'water', 'proddays', 'location', 'depth', 'pool']
    # targets = ['oil', 'water', 'wor']
    targets = ['oil']
    # import pdb; pdb.set_trace()
    features = ['oil', 'water', 'proddays', 'location', 'depth', 'pool']
    log_file = open('log_batch.txt', 'wt')
    for target in targets:
        for state in states:
            table_name = 'FFR_{}_{}_{}_{}_cont'.format(state, 1000, 36, target)
            try:
                main_ffr(target, state, 'bigprod_ts', features, 36, table_name)
                log_file.write(table_name + '\n')
                log_file.flush()
            except:
                print table_name+" failed."
    log_file.close()

if __name__ == "__main__":
    # run_all()
    # import pdb;
    # pdb.set_trace()
    main_ffr(target='oil',
             state='CA',
             source='bigprod_ts',
             features=['oil', 'water', 'proddays', 'location', 'depth', 'pool'],
             length=24,
             table_name='fore_ffr_ca_oil_24')
