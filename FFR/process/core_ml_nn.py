"""
This file implements the core machine learning part of the FFR algorithm.
The db interface and auxilliary functions are in 'helpers_ffr' module.

The main function in this file is `ffr()`.

Note that the FFR algorithm cannot be understood without the mathematical
 description provided in the following link:
http://terra.ai:8090/display/forteam/FFR+and+SSM

Parameters and settings:
# STATE:    the state to be analyzed.
# WINP:     length of the sequence used for training
# WINQ:     max length of the testing period
# N_INDUCE:  number of inducing points in FITC
# SM:       number of time steps used for linear regression in
#           finding the bias term.
# TEST_DATE: start date for testing period
"""

from __future__ import division
import os
import warnings
from collections import OrderedDict
from multiprocessing import Pool
from datetime import datetime
import cPickle as pk
import pdb

import numpy as np
from numpy.linalg import solve, norm
from scipy.spatial.distance import cdist
import matplotlib.pyplot as plt
from tqdm import tqdm   # For progress bar

import helpers_pg as hpf

warnings.filterwarnings("ignore")  # Not a very brilliant idea!

FEATURES = OrderedDict(zip(['oil', 'water', 'gas', 'proddays', 'location', 'depth', 'pool'],\
                        ['ts', 'ts', 'ts', 'ts', 'vector', 'scalar', 'category']))
EPS = 1e-4

def load_data_forward(r_query, s_query):
    """
    Globally Loading the Data for the forward forecasting.
    """
    global data, LENS
    data, test_it = hpf.read_from_db(r_query, s_query)
    LENS = hpf.getLength(data['oil'])
    return test_it

def preprocess(target, log):
    """ This function implements the most basic preprocessing for the data.
    It does the followings:
    # Makes sure that all of the values are finite.
    # Converts monthly oil and water to daily via dividing them by `proddays`.
    # Takes the log of oil and water.

    NOTE: This function is the only function that changes the global variable `data`
    after 'load_data()'.
    """
    if target not in FEATURES.keys():
        FEATURES[target] = 'ts'
    for i in range(len(data['oil'])):
        # For multi-dimensional forecasts
        if target is 'wor':
            data['wor'].append(data['water'][i]/data['oil'][i])
        elif target is 'cumoil':
            data['cumoil'][i].append(np.cumsum(data['oil'][i]))
        elif target is 'cumwater':
            data['cumwater'][i].append(np.cumsum(data['cumwater'][i]))
        # Main variables
        data['proddays'][i][np.isnan(data['proddays'][i])] = 0
        # data['oil'][i] /= data['proddays'][i]        # This makes oil per day (oil_pd)
        # data['water'][i] /= data['proddays'][i]      # This makes water per day (water_pd)
        # Take log of oil
        data['oil'][i] = np.log10(1+data['oil'][i])  # log(1+x) to make sure that it is invertable
        data['oil'][i][~np.isfinite(data['oil'][i])] = 0
        data['water'][i] = np.log10(1+data['water'][i]) # log(1+x) to make sure invertablity
        data['water'][i][~np.isfinite(data['water'][i])] = 0
        data['gas'][i] = np.log10(1+data['gas'][i]) # log(1+x) to make sure invertablity
        data['gas'][i][~np.isfinite(data['gas'][i])] = 0
        data[target][i][~np.isfinite(data[target][i])] = 0

def notfailed(i, t, winp):
    """
    A simple way to see if the well has failed or not.
    """
    if sum(data['oil'][i][t:t+winp] == 0)*1.0/winp > 0.3:
        return False
    else:
        return True

def kernel(we1, we2, setting, tlen):
    """
    Computes the kernel between two wells 'we1' and 'we2' given the parameters in 'params'.
    """
    params = setting['params']['params']
    out = 0
    for (j, item) in enumerate(FEATURES.keys()):
        if FEATURES[item] == 'ts':
            temp = cdist(data[item][we1[0]][we1[1]:we1[1]+tlen][None, :], \
                data[item][we2[0]][we2[1]:we2[1]+tlen][None, :])[0, 0]/np.sqrt(tlen)
        elif FEATURES[item] == 'vector':
            temp = cdist(data[item][we1[0]][None, :], data[item][we2[0]][None, :])[0, 0]
        elif FEATURES[item] == 'scalar':
            temp = (data[item][we1[0]] - data[item][we2[0]])**2
        elif FEATURES[item] == 'category':
            temp = (data[item][we1[0]] != data[item][we2[0]])
        out += params[0][j]*np.exp(-temp*params[1][j])
    return out

def testable(i, winp, winq, test_date):
    if (test_date-winp in data['dates'][i].tolist()) and (test_date in data['dates'][i].tolist()):
        tindex = test_date-winp-min(data['dates'][i])
        pred_len = min(LENS[i], tindex+winq+winp) - (tindex+winp)       # How long we can predict
        if (pred_len > 0) and notfailed(i, tindex, winp):
            return True
    return False

def match_ts(test_id, train_id, test_date, setting, t_train, tlen, winq):
    """
    Knowing that the most critical element is the target time series,
    we match with the best similarity in the curves.
    This should be changed to the kernel computation.
    ### Focus on the q and others.
    """
    t_test = np.where(data['dates'][test_id] == test_date)[0][0]-tlen
    train_q = np.array(data[setting['target']][train_id][t_train+tlen-1:t_train+tlen+winq])
    score = kernel((test_id, t_test), (train_id, t_train), setting, tlen)
    return score, train_q

def best_match(test_id, train_id, test_date, setting, winq, winr):
    """ Search through train id, if you find enough data,
    return the best subsequence (i, t), otherwise, return None.
    """
    # Check if the training well has enough data after the test_date
    # len_future = np.where(data['dates'][train_id] >= test_date)[0].shape[0]
    # if len_future < winq:
    #     return None
    # # Exclude the very failed wells
    # tr_t = np.where(data['dates'][train_id] == test_date)[0][0]
    # if len(np.where(data['oil'][train_id][tr_t:tr_t+winq] == 0)[0]) > 0.3*winq:
    #     return None
    # Check if the training well has data before test_date
    # How much data we have in the test example
    len_train = np.where(data['dates'][train_id] < test_date)[0].shape[0] - winq
    if len_train < winr:
        # We do not have much data
        return None   # Not enough data
    elif len_train == winr:
        # Here there is only one possible match.
        score, qq = match_ts(test_id, train_id, test_date, setting, 0, winr, winq)
        return {'i_&_t':(train_id, 0), 'qs':qq, 'score':score}
    else:
        # Compute the best subsequence
        scores, times, qqs = list(), list(), list()
        for t in range(len_train-winr):
            ss, qq = match_ts(test_id, train_id, test_date, setting, t, winr, winq)
            scores.append(ss)
            times.append(t)
            qqs.append(qq)
        sid = np.argmax(np.array(scores))
        return {'i_&_t':(train_id, times[sid]), 'qs':qqs[sid], 'score':scores[sid]}

def get_it(test_id, setting, test_date, winq):
    """
    Taha: Can you comment this more? I am thinking of taking over this functionality. Let me know what you think.
    I think I might need a little help understanding the logic behind this.
    This is where we can put in rules right?
    """
    test_index = np.where(data['dates'][test_id] == test_date)[0][0]
    test_q = np.zeros((winq+1,))
    tindex = test_date-min(data['dates'][test_id])-1
    len_future = min(max(data['dates'][test_id]) - test_date + 2, winq+1)
    test_q[:len_future] = data[setting['target']][test_id][tindex:tindex+len_future]
    return test_index, test_q, tindex

def visualize(test_id, test_index, train_its, train_qs, test_q, test_date, setting):
    plt.figure(1)                # the first figure
    len_test = test_date - min(data['dates'][test_id])
    test = np.concatenate((data['oil'][test_id][test_index:test_index+len_test], test_q))
    plt.plot(test, 'r')
    for (j, (i, t)) in enumerate(train_its):
        train = np.concatenate((data['oil'][i][t:t+len_test], train_qs[j]))
        plt.plot(train, 'b')
    plt.show()

def write_similar_wells(test_it, train_its, scores, winr, setting):
    """
    Writes the wells similar to the test well in an appropirate directory.
    """
    directory = './similar_wells/{}/'.format(setting['target'])
    if not os.path.exists(directory):
        os.makedirs(directory)
    with open(directory+str(data['padID'][test_it[0]]), 'wt') as fh:
        fh.write('Similarity length: {},\t Forecasting length: {}\n'.format(winr, setting['winq']))
        fh.write('\n'.join(['{}\t{}\t{}'.format(data['padID'][x[0][0]], \
            hpf.add_months(data['dates'][x[0][0]][x[0][1]]), x[1]) for x in zip(train_its, scores)]))

def format_similar_wells(test_it, train_its, scores, winr, setting):
    """
    Writes the wells similar to the test well in an appropirate directory.
    Taha - Can you give an example of how this exactly is returned? I think this might be useful right?
    """
    if not setting['sim']['status']:
        return None
    n = setting['sim']['limit']
    return ','.join(["({},{},'{}'::date,{},{},{})".format(data['padID'][test_it[0]], data['padID'][x[0][0]], \
        hpf.add_months(data['dates'][x[0][0]][x[0][1]]), x[1], winr, setting['winq']) for x in zip(train_its[:n], scores[:n])])


def make_linear(train_qs, test_q):
    train_qs = np.power(10, train_qs) - 1
    test_q = np.power(10, test_q) - 1
    return train_qs, test_q
    
def search_train(test_id, setting, test_date, winq):
    train_points, scores, train_qs_all = list(), list(), list()
    nlen = len(data['padID']) if setting['limit'] is None else setting['limit']
    winr = min(test_date - min(data['dates'][test_id]), setting['winr'])
    for i in xrange(nlen):
        if i==test_id:
            continue
        match_result = best_match(test_id, i, test_date, setting, winq, winr)
        if match_result is not None:
            train_points.append(match_result['i_&_t'])
            scores.append(match_result['score'])
            train_qs_all.append(match_result['qs'])
    # Select the most similar wells
    index = np.argsort(np.array(scores))[::-1][:setting['n_train']].tolist()
    train_its = [train_points[i] for i in index]
    train_qs = [train_qs_all[i] for i in index]
    scores = [scores[i] for i in index]
    # Extrat the info from the test point
    test_index, test_q, pred_len = get_it(test_id, setting, test_date, winq)
    # visualize(test_id, test_index, train_its, train_qs, test_q, test_date, setting)
    return train_its, np.array(train_qs), (test_id, test_index), test_q, scores, winr

def do_gp(train_its, train_qs, test_it, test_q, offset, scores, setting, winr):
    """ Using GP to predict on the test example.
    """
    # pdb.set_trace()
    params = setting['params']
    yhat = np.zeros((1, test_q.shape[0]-1))
    shat = np.zeros((1, test_q.shape[0]-1))
    if train_qs.size == 0:
        print "No training well for well {} has been found".format(data['padID'][test_it[0]])
        return yhat, test_q[1:][None, :], shat
    # write_similar_wells(test_it, train_its, scores, winr, setting)
    similar = format_similar_wells(test_it, train_its, scores, winr, setting)
    # train_qs, test_q = make_linear(train_qs, test_q)

    # Compute the kernels
    Kxx = np.array([[kernel(x, y, setting, winr) for x in train_its] for y in train_its])
    Kxx = (Kxx+Kxx.T)/2
    kxz = np.array(scores)[:, None]
    kzz = kernel(test_it, test_it, setting, winr)
    # Biases
    b_train = np.array(train_qs)[:, 0][:, None]
    b_test = test_q[0]
    offset = 0 if np.isnan(offset) else offset-b_test
    # Do GP
    for i in range(1, test_q.shape[0]):
        sigma = params['sigma'][0] + params['sigma'][1]*np.power(i, params['sigma'][2])
        Kinvk = solve(Kxx+sigma*np.eye(Kxx.shape[0]), kxz)
        yhat[0, i-1] = np.dot(Kinvk.T, (train_qs[:, [i]]-b_train)) + b_test + offset
        shat[0, i-1] = np.sqrt(kzz + sigma - np.dot(kxz.T, Kinvk))[0, 0]
    return yhat, test_q[1:][None, :], shat, similar

def evaluate_error(yture, yhat, measure='mean'):
    """
    Evaluation of the error of the forecasting.
    """
    errs = np.sum(((yhat-yture)**2)*(yture > 0), axis=1)
    norms = np.sum(yture**2, axis=1)
    nnz = np.where(norms != 0)[0]
    if measure == 'median':
        return np.median(np.sqrt(errs[nnz])/np.sqrt(norms[nnz]))
    elif measure == 'weighted':
        weights = norms/sum(norms)
        return np.sqrt(sum(weights[nnz]*errs[nnz]/norms[nnz]))
    else:
        return np.sqrt(np.mean(errs[nnz]/norms[nnz]))

def select_test_wells(setting, test_date, winq):
    """
    One way to generate test wells. This is for batch back run setting.
    """
    test_it = list()
    # Make this loop parallel
    # Taha ^ How?- Take counter out of function and as input then pool.imap(select_test_wells,setting, test_date=TEST_DATE, winq=WINQ, i) ? 
    for i in range(len(data['padID'])): 
        if testable(i, setting['winp'], winq, test_date):
            test_it.append((i, test_date, winq, np.nan))
    print "Number of test wells: {}\n".format(len(test_it))
    return test_it

def process_test_wells(test_it, winq):
    """
    This is for the forward tests.
    """
    tests = list()
    for (index, test_date) in test_it:
        tests.append((index, test_date, winq, np.nan))
    return tests

def smooth(yhat, setting):
    # Moving
    enum = np.arange(yhat.shape[1])
    smoother = np.exp(-abs(enum[None, :] - enum[:, None])/setting['smth'])
    #Taha - What happens when we increase smth to beyond 1? Also what happens on the first tie in point to the smoothing?
    smoother /= np.sum(smoother, axis=0, keepdims=True)
    return np.dot(yhat, smoother)

def run_ffr_once(inp):
    test_id, test_date, winq, offset, setting = inp
    try:
        train_its, train_qs, test_it, test_q, scores, winr = search_train(test_id, setting, test_date, winq)
        yhat, y_test, shat, similar = do_gp(train_its, train_qs, test_it, test_q, offset, scores, setting, winr)
        yhat = smooth(yhat, setting)
        shat = smooth(shat, setting)
        return yhat, y_test, shat, similar
    except Exception, err: # Fail gracefully.
        print "Failed to forecast well {}".format(data['padID'][test_id])
        print err
        _, test_q, _ = get_it(test_id, setting, test_date, winq)
        y_test = test_q[None, 1:]
        return np.zeros(y_test.shape), y_test, np.ones(y_test.shape), ''

def run_ffr(test_it, setting):
    """
    This function is the main API function to call for FFR to forecast.

    Inputs:
    # data: a global variable, dictionary of lists. This includes the preprocessed
            data accroding to the global variable 'FEATURES'. See the functions
            load_data() and preprocess() for an example of creating the data.
    # test_it: list of testing scenarios:
               It is a list of tuples. Each tuple has 4 elements: (id, test_date, winq, offset):
               id: the index of the test well in the data.
               test_date: the starting date of the forecast. Converted to an inteager using
                     hpf.diff_month().
               winq: The length of forecast in months.
               offset: The initial value in the forecast. Leave it as np.nan if you don't know it.
    # setting: A dictionary that has the settings. It has the following keys:
            'target': A string, specifying the target variable, such as 'oil' or 'water'.
            'table_name': A string. If you want to name a table provide it here,
                          otherwise leave it as None to use the default tablename.
            'params': The list of kernel parameters.
            'n_train': An integer, specifies the number of wells used in the GP step.
            'limit': An integer. If we want to speedup the process, setting this number
                     will limit the search to the first 'limit' number of wells.
                     If you leave it as None, it will search the entire training set.

    Outputs:
            yhat: A list, which contains the forecast for the given well.
            y_test: A list of the corresponding observed values, if possible.
            shat: A list of standard deviations for the forecasts.
    """
    pool = Pool()
    inputs = list()
    
    # Taha - Comment what these items are here exactly
    for item in test_it:
        inputs.append((item[0], item[1], item[2], item[3], setting))
    print "Running FFR ..."
    results = [res for res in tqdm(pool.imap(run_ffr_once, inputs))]
    pool.close()

    yhat = [x[0] for x in results]
    y_test = [x[1] for x in results]
    shat = [x[2] for x in results]
    similars = [x[3] for x in results]
    return yhat, y_test, shat, similars

def run_ffr_seq(test_it, setting):
    """
    This is only for debug purposes.
    Taha: Can you describe how to use it?
    
    """
    yhat, y_test, shat, similars = list(), list(), list(), list()
    for item in tqdm(test_it):
        pdb.set_trace()
        a1, a2, a3, a4 = run_ffr_once( (item[0], item[1], item[2], item[3], setting) )
        yhat.append(a1)
        y_test.append(a2)
        shat.append(a3)
        similars.append(a4)
    yhat, y_test, shat = np.array(yhat), np.array(y_test), np.array(shat)
    return yhat, y_test, shat, similars

def ffr(train_query, test_query, setting):
    """
    Taha Comment 
    This is a non-parallel version of ffr
    Taha: Do we need this? What is is used for besides possibly running the code on windows where the parallelism breaks. 
    """
    np.random.seed(0)   # For reproducability
    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Starting Forward FFR.")
    log = True if setting['target'] in ['oil', 'water', 'gas'] else False
    setting['log'] = log
    
    # The similarity hyperparameters
    if setting['params'] is None:
        setting['params'] = [[0.05, 0.01, 0.05, 0, 0, 0], [1, 1, 1, 0, 0, 0]]
        setting['sigma'] = [2, 0.1, 0.2]

    tests = load_data_forward(train_query, test_query)
    print "Pre-processing the data."
    preprocess(setting['target'], setting['log'])

    # The main forecasting part
    test_it = process_test_wells(tests, winq=setting['winq'])
    yhat, y_test, shat, similars = run_ffr(test_it, setting)
    
    # These should be changed for the general winq setting
    yhat, y_test = np.vstack(tuple(yhat)).astype(float), np.vstack(tuple(y_test)).astype(float)
    shat = np.vstack(tuple(shat)).astype(float)

    target = setting['target']
    print "Postprocessing the data..."
    data_out = hpf.convert_data_nn(data, yhat, shat, test_it, setting)
    if 'table_name' in setting:
        table_name = setting['table_name']
    else:
        table_name = 'FFR_NN_{}_{}_{}_{}'.format(setting['state'], setting['n_train'], setting['winp'], target)
    print "Writing to db in batches..."
    hpf.write2pg(data_out, table_name, target)
    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Finished FFR.")
    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Forecasts are in table {}.".format(table_name))

    hpf.write_sim_db(similars, setting)
    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Finished writing similar wells.")


def ffr_back(train_query, test_query, setting):
    """
    This is the top function that calls functions in the right sequence and
    prints the proper output in the screen. Please follow the print functions'
    output to see this function's objective.
    """
    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Starting FFR for {} on state {}.".format(
        setting['target'], setting['state']))
    
    # Taha?
    log = True if setting['target'] in ['oil', 'water', 'gas'] else False
    # log = False
    setting['log'] = log

    # The similarity hyperparameters
    if setting['params'] is None:
        setting['params'] = [[0.05, 0.01, 0.05, 0, 0, 0], [1, 1, 1, 0, 0, 0]]
        setting['sigma'] = [2, 0.1, 0.2]

    np.random.seed(0)   # For reproducability
    load_data_forward(train_query, test_query)
    print "Pre-processing the data."
    preprocess(setting['target'], setting['log'])

    # The main forecasting part
    # Navjot - In merge, allow setting to only run top 100 wells or so for testing
    # Config
    test_it = select_test_wells(setting, test_date=setting['test_date'], winq=setting['winq'])[:100]
    yhat, y_test, shat, similars = run_ffr(test_it, setting)
    
    # These should be changed for the general winq setting
    # Taha - how so?
    yhat, y_test = np.vstack(tuple(yhat)).astype(float), np.vstack(tuple(y_test)).astype(float)
    shat = np.vstack(tuple(shat)).astype(float)
    hpf.write_sim_db(similars, setting)
    
    target = setting['target']
    print "Noramlized Median for {}:\t{}.".format(target, evaluate_error(y_test, yhat, 'median'))
    print "Noramlized RMSE for {}:\t{}.".format(target, evaluate_error(y_test, yhat, 'mean'))
    print "Noramlized W-RMSE for {}:\t{}.".format(target, evaluate_error(y_test, yhat, 'weighted'))
    print "Postprocessing the data..."
    data_out = hpf.convert_data_nn(data, yhat, shat, test_it, setting)
    if 'table_name' in setting:
        table_name = setting['table_name']
    else:
        table_name = 'FFR_NN_{}_{}_{}_{}'.format(setting['state'], setting['n_train'], setting['winp'], target)
    print "Writing to db in batches..."
    hpf.write2pg(data_out, table_name, target)
    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Finished FFR on state {}.".format(setting['state']))
    print '\x1b[%sm%s\x1b[0m' % ('31;1', "Forecasts are in table {}.".format(table_name))

if __name__ == "__main__":

    # Taha - This is depricated right? Can we get rid of it?
    PAR = {'params': [[0.0649, 0, 0.0102, 0.0122, 0, 0, 0.0109], 
            [0.9913, 0.9933, 0.9956, 0.9943, 0.0056, 8.34-05, 0.0643]], 
            'sigma': [0.0026, 0.027, 0.0581]}
    # Oil, log
    # PAR = {'params': [[0.3627, 0, 0, 0, 0.2182, 0.2166, 0.2181], 
    #                   [0.8544, 0.9991, 0.9961, 0.9994, 0.0148, 0, 0.0084]], 
    #        'sigma': [1e-05, 0.021259, 0.382775]}
    # Gas, log
    # PAR = {'params': [[0.0201, 0.0052, 0.6026, 0.0066, 0.1627, 0.1820, 0.1630], 
    #         [0.9956, 0.9910, 0.9501, 0.9976, 0.0306, 0.0084, 0.0267]], 
    #        'sigma': [0.0083, 0.0150, 0.0132]}
    # Water, log
    # PAR = {'params': [[0.0516, 0.7352, 0.0015, 0, 0.3369, 0.3200, 0.3435], 
    #        [0.9808, 0.5597, 0.9997, 0.9995, 0.0141, 0.00054, 0.00015]], 
    #        'sigma': [7.71e-05, 0.03678, 0.5311]}
    WINP = 3 # This is the minimum length of time series that will be chosen for training.
    WINR = 48 # This is the maximum length of time series that will be chosen for training.
    WINQ = 72 # This is the lenght of future forecasting
    TEST_DATE = hpf.diff_month(datetime.strptime('2010-01-01', '%Y-%m-%d'))
    STATE = 'RAND'
    SETTING = {'target':'oil', 'state':STATE, 'table_name':'ffr_NDK_back_nn_oil_smooth', 'params':PAR,
               'test_date':TEST_DATE, 'n_train':500, 'winp':WINP, 'winq':WINQ, 'winr':WINR,
               'limit':None, 'smth':1, 'n_sim':100}
    train_query = """Select "API_prod", report_date_mo, oil_pd, water_pd, gas_pd, prod_days_up, lat, longi, depth, pool_name from public_data.bigprod_ts where "state_API" = 33"""
    test_query = """Select "API_prod", '2010-01-31' as forecast_start_date from public_data.bigprod_ts where "API_prod" in(Select distinct "API_prod" from public_data.northdakota_prod where oil_mo != 0 and
                    report_date_mo in('2009-10-31', '2009-11-30', '2009-12-31', '2010-01-31'))"""
    ffr_back(train_query, test_query, SETTING)
    # ffr_forward(train_query, test_query, SETTING)
