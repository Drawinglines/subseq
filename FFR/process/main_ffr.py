"""
This is the top file for calling FFR.
See the documentation of 'main_ffr()' for how to use it.
"""
# System packages
from __future__ import division
from collections import OrderedDict
from datetime import datetime
import sys
from time import time
import pdb

# Local packages
import helpers_pg as hpf
import core_ml_nn as ml
import sgd_ffr as opt

# Local to be moved?
import myjson as json

# fetl packages
from helpers.myjson import update_kps, write_kps_to_config
from load.tabt import tabt_fore_run_init, tabt_fore_run_update, tabt_fore_run_fin


def main_ffr(config):
    """
    This is the main function implementing the entire pipeline and
    connecting different pieces of FFR code.
    Please follow the inline comments.
    """
    # Create a unique time signature for all runs.
    time_sign = '_'+str(int(time()))+'_'
    for target in config['global']['products_fore']:

        # Test to see if the config file has been named properly
        if config['config']['name'] is None:
            raise KeyError('No name specified for config / run')

        config_name = config['config']['name'] + '.json'
        run_id = config['config']['name'] + time_sign + target
        # If we want to specify a special table name, we can do it here.
        # The run id is always unique
        if config['global']['table_name'] is "":
            table_name = config['global']['table_name'] + time_sign + target
        else:
            table_name = config['global']['table_name']

        # Initialize table tracker
        tabt_fore_run_init(run_id, config)

        # Check if the details of the target specified.
        if target not in config:
            raise KeyError('Parameters of target {} are not specified.'.format(target))

        train_query = config['global']['source']['train_sql']
        test_query = config['global']['source']['test_sql']
        # If we are asked to do optimization, call the optimizer.
        if config[target]['kernel']['run']['opt']:
            features = OrderedDict(zip(config[target]['features'], config[target]['feature_types']))
            opt_set = {'query':train_query,
                       'features':features,
                       'target':target,
                       'winq':config['global']['winq']//10,   #  Fix this
                       'winp':config['global']['winp'],
                       'test_date':hpf.diff_month(datetime.strptime('2010-01-31', '%Y-%m-%d')),
                       'batch_size':50,
                       'n_epoch':50,
                       'learn_rate':1e-3,
                       'l2reg':0,
                       'init':config[target]['kernel']['kps'],
                       'track_opt':config[target]['kernel']['run']['track_opt'],
                       'run_id':run_id}

            # Call the optimization
            best_param = opt.optimize(opt_set)
            config = update_kps(config, target, best_param)

            print config_name
            if config[target]['kernel']['run']['write_to_config']:
                print 'Writing kps to config'
                write_kps_to_config(config_name, config)

            if config[target]['kernel']['run']['write_to_db']:
                print "Writing kps to DB"

            tabt_fore_run_update(table_name, run_id, config)
        # Call the forecasting.
        fore_set = {'target':target,
                    'table_name':config['global']['table_name']+time_sign+target,
                    'params':config[target]['kernel']['kps'],
                    'test_date':hpf.diff_month(datetime.strptime(config['global']['test_date'], '%Y-%m-%d')),
                    'n_train':config['global']['n_train'],
                    'winp':config['global']['winp'],
                    'winq':config['global']['winq'],
                    'winr':config['global']['winr'],
                    'limit':None,
                    'smth':config['global']['smoothing'],
                    'sim':config['global']['sim_wells']}
        ml.ffr(train_query, test_query, fore_set)

    # Call the extender.
    for target in config['global']['products_fore']:
        table_name = config['global']['table_name']+time_sign+target
        ext_set = {'mode':config[target]['extension']['mode'],
                   'par':config[target]['extension']['parameter'],
                   'limit':config[target]['extension']['limit'],
                   'go_up':config[target]['extension']['go_up'],
                   'max_len':20}
        hpf.extend(table_name, ext_set)
        tabt_fore_run_fin(run_id)

def parse_config(config_name):
    with open(config_name, 'rt') as conf:
        config = json.json_load_byteified(conf)
    # config['oil']['kernel']['kps']['params'] = [config['oil']['kernel']['kps']['params']['c'], \
    #                                             config['oil']['kernel']['kps']['params']['d']]
    # config['water']['kernel']['kps']['params'] = [config['water']['kernel']['kps']['params']['c'], \
    #                                             config['water']['kernel']['kps']['params']['d']]
    # config['gas']['kernel']['kps']['params'] = [config['gas']['kernel']['kps']['params']['c'], \
    #                                             config['gas']['kernel']['kps']['params']['d']]
    return config

if __name__ == "__main__":
    # global config_name
    # config_name = sys.argv[1]
    # CONFIG = parse_config(config_name)
    CONFIG = parse_config(sys.argv[1])
    main_ffr(CONFIG)
