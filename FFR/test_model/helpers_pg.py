"""
This module includes a set of helper functions for FFR:
# Reading from database
# Pre-processing the data
# Post-processing the data
# Writing into database

Parameters and settings:
# initDate      : A reference date for converting dates to month differences
# indexDate     : start date for testing period
# stateTable    : Dictionary of state names and IDs.
"""
from __future__ import division
from collections import defaultdict
import pdb
import psycopg2
import numpy as np
from datetime import datetime, date
from calendar import monthrange
import cPickle as pk
from tqdm import tqdm   # For progress bar
import pdb
from sklearn.linear_model import Ridge

initDate = datetime.strptime('1900-01-01' , '%Y-%m-%d')
indexDate = datetime.strptime('2008-01-01' , '%Y-%m-%d')
stateTable = {'CA':4, 'CO':5, 'NM':30, 'NDK':33, 'OK':35, 'UT':43, 'AK':50, 'WY':49, 'RAND':0}
config = {'host':'terra.c4lstuf6msdr.us-west-2.rds.amazonaws.com', 'port':'5432', 'user':'terra', 'password':'fundus500k'} 
def diff_month(d1):
    """
    Calculates the difference (d1-initDate) in terms of number of months in between them.
    """
    try:
        if not isinstance(d1, datetime): # Parameter overloading
            d1 = datetime.strptime(d1, '%Y-%m-%d')
        return (d1.year - initDate.year)*12 + d1.month - initDate.month
    except:
        pdb.set_trace()
        print "Exception.  The aregument was {}.".format(d1)
        return 0

def diff_month_date(d1):
    return (d1.year - initDate.year)*12 + d1.month - initDate.month

def add_months(months):
    """
    Returns the date for several months after the initDate. 
    """
    month = initDate.month - 1 + months
    year = int(initDate.year + month / 12 )
    month = month % 12 + 1
    return date(year,month,monthrange(year,month)[1])  # Reporting on the last day of the month

def getLength(oil):
    """
    Calculates the length of oil time series.  
    Here, the last data point of the time series is the last non-zero production month.
    """
    N = len(oil)
    lens = [x.shape[0] for x in oil]
    for i in range(N):
        while oil[i][lens[i]-1] == 0:
            lens[i] -= 1
            if lens[i] == 0: break
    return lens

def mynum(func, a, replacement=0):
    ''' This function is only a way to get around the unexpected data while converting it to type specified by 'func' argument.
    You may want to write a more sophisticated function for treatment of bad entries.'''
    try:
        return func(a)
    except:
        # print('Expected float, got {} instead'.format(type(a)))
        return replacement

def handleMissing(dates, oil, water, gas, days):
    """
    This function handles the missing data points by inserting zeros in the missing months.
    """
    new_dates = np.arange(min(dates), max(dates)+1) 
    new_oil, new_water, new_gas, new_days = [np.zeros(new_dates.shape) for _ in range(4)]
    new_oil[dates-min(dates)] = oil
    new_water[dates-min(dates)] = water
    new_days[dates-min(dates)] = days
    new_gas[dates-min(dates)] = gas
    return new_dates, new_oil, new_water, new_gas, new_days

def encodePools(names):
    uni = list(set(names))
    nameMap = dict(zip(uni, xrange(len(uni))))
    return [nameMap[x] for x in names]

def check_lens(items):
    """
    Return False if all of them have the same length.
    """
    lens = set([len(x.split(',')) for x in items[1:]])
    if len(lens) == 1:
        return False
    else:
        print items[0]
        return True

def read_from_db(train_query, test_query):
    """
    This function reads the data from the database and converts them to a defaultdict format.
    It also performs some mild preprocessing on the data such as convertion of the dates to diff
    months and handling missing data with handleMissing() function.
    """
    # Establish the connection
    conn = psycopg2.connect(database='terra', user=config['user'], password=config['password'], host=config['host'], port=config['port'])
    cursor = conn.cursor()
    cursor.execute(train_query)
    row = cursor.fetchall()
    data = defaultdict(list)
    print "Reading from the database:"
    for (i, item) in enumerate(tqdm(row)):
        # "API_prod", report_date_mo, oil_pd, water_pd, gas_pd, prod_days_up, lat, longi, depth, pool_name
        if check_lens([item[x] for x in [0,1,2,3,4,5]]):
            continue
        oil = np.array([mynum(float, x) for x in item[2].split(',')])
        if oil.sum() == 0:
            continue
        dates  = np.array([diff_month(x) for x in  item[1].split(',')])
        index = np.argsort(dates)
        dates = dates[index]
        water = np.array([mynum(float, x) for x in item[3].split(',')])[index]
        gas = np.array([mynum(float, x) for x in item[4].split(',')])[index]
        days = np.array([mynum(int, x) for x in item[5].split(',')])[index]
        dates, oil, water, gas, days = handleMissing(dates, oil[index], water, gas, days)

        data['padID'].append( item[0] )
        data['dates'].append( dates )
        data['oil'].append( oil )
        data['water'].append( water )
        data['gas'].append( gas )
        data['proddays'].append( days )
        if (item[6] is None) or (item[7] is None):
            data['location'].append( np.array([0, 0]) )
        else:
            data['location'].append( np.array([mynum(float, item[5]), mynum(float, item[6])]) )
        if item[8] is None:
            data['depth'].append(0)
        else:
            data['depth'].append( mynum(float, item[7]) )
        data['pool'].append(item[9])

    # Coding the pool names into numerical categories
    data['pool'] = encodePools( data['pool'] )

    if test_query is None:
        cursor.close()
        conn.close()
        return data, None

    cursor.execute(test_query)
    row = cursor.fetchall()
    indexes, test_dates, counter = list(), list(), 0
    for x in row:
        if x[0] in data['padID']:
            indexes.append(data['padID'].index(x[0]))
            test_dates.append(diff_month(str(x[1])))
        else:
            counter += 1
    print "{} out of {} test wells are not found in the training".format(counter, len(row))

    cursor.close()
    conn.close()
    return data, zip(indexes, test_dates)


def readFromDB(stateID=[33]):
    """
    This function reads the data from the database and converts them to a defaultdict format.
    It also performs some mild preprocessing on the data such as convertion of the dates to diff
    months and handling missing data with handleMissing() function.
    """
    # Establish the connection
    conn = psycopg2.connect(database='terra', user=config['user'], password=config['password'], host=config['host'], port=config['port'])
    cursor = conn.cursor()
    if stateID[0] == 0:   # Random selection
        query = """Select "API_prod", report_date_mo, oil_mo, wtr_mo, prod_days, lat, longi, depth,
        pool_name from public_data.rand_ts_latest;"""
    else:
        query = """Select "API_prod", report_date_mo, oil_mo, wtr_mo, prod_days, lat, longi, depth,
        pool_name from public_data.bigprod_ts where "state_API" in ({});""".format(','.join([str(x) for x in stateID]))

    cursor.execute(query)
    row = cursor.fetchall()
    data = defaultdict(list)
    print "Reading from the database:"
    for (i, item) in enumerate(tqdm(row)):
        if check_lens(item[:5]):
            continue
        oil = np.array([mynum(float, x) for x in item[2].split(',')])
        if oil.sum() == 0:
            continue
        dates  = np.array([diff_month(x) for x in  item[1].split(',')])
        index = np.argsort(dates)
        dates = dates[index]
        water = np.array([mynum(float, x) for x in item[3].split(',')])[index]
        days = np.array([mynum(int, x) for x in item[4].split(',')])[index]
        dates, oil, water, days = handleMissing(dates, oil[index], water, days)

        data['padID'].append( item[0] )
        data['dates'].append( dates )
        data['oil'].append( oil )
        data['water'].append( water )
        data['proddays'].append( days )
        if (item[5] is None) or (item[6] is None):
            data['location'].append( np.array([0, 0]) )
        else:
            data['location'].append( np.array([mynum(float, item[5]), mynum(float, item[6])]) )
        if item[7] is None:
            data['depth'].append(0)
        else:
            data['depth'].append( mynum(float, item[7]) )
        data['pool'].append(item[8])

    # Coding the pool names into numerical categories
    data['pool'] = encodePools( data['pool'] )

    cursor.close()
    conn.close()
    return data


def format_forecast_nn(shape, hat, ln, jn, log):
    """
    This is a post-processing function.
    # It removes the part of forecast that is beyond the length of the actual time series.
    # The previous one does not happen anymore. For that we use hat[:ln] instead of hat.
    # Lines 138 and 141.
    # It reverses the log operation in the preprocessing step.
    # It ensures that the output is non-negative.
    """
    fore = np.nan*np.empty(shape)
    if log:
        temp = np.power(10, hat[:ln])-1
    else:
        temp = hat[:ln]
    fore[jn[1]:jn[1]+ln] = (temp*(temp >= 0))
    return fore

def confidence(yhat, shat):
    """
    Assuming Gaussian noise, we use the z-scores to find 0.1 and 0.9 percentiles of the forecasts.
    """
    z = 1.645
    yhat10 = yhat - z*shat
    yhat90 = yhat + z*shat
    return yhat10, yhat90

def convert_data_nn(dataIn, yhat, shat, test_it, setting):
    """
    This function converts the data to the right defaultdict format for writing into the db.
    It reverses the preprocessing operations and uses formatForecast() to cut the
    time series beyond the length of actual time series.
    Also, it invokes confidence() to calculate the percentile curves.
    """
    target, winp, winq, log = setting['target'], setting['winp'], setting['winq'], setting['log']
    data = defaultdict(list)
    for (i, (ind, test_date, tlen, _)) in enumerate(tqdm(test_it)):
        # tlen = min(len(dataIn[target][ind]) - (ts+winp), winq) 
        ts = np.where(dataIn['dates'][ind]==test_date)[0][0]
        tot_len = ts+tlen
        data['padID'].append(dataIn['padID'][ind])
        temp = np.ones((tot_len,))*np.nan
        temp[:len(dataIn['dates'][ind])] = dataIn['dates'][ind][:tot_len]
        for j in range(len(dataIn['dates'][ind]), len(temp)):
            temp[j] = temp[j-1] + 1
        data['date'].append( np.array( [add_months(x) for x in temp.astype(int).tolist()] ) )
        temp = np.ones((tot_len,))*np.nan
        temp[:len(dataIn['proddays'][ind])] = dataIn['proddays'][ind][:tot_len]
        data['proddays'].append(temp)
        temp = np.ones((tot_len,))*np.nan
        temp[:len(dataIn[target][ind])] = dataIn[target][ind][:tot_len]
        if log:
            data[target].append(np.power(10, temp)-1)
        else:
            data[target].append(temp)

        shape = temp.shape
        yhat10, yhat90 = confidence(yhat, shat)
        data[target+'_fore'].append( format_forecast_nn(shape, yhat[i,:], tlen, (ind, ts), log) )
        data[target+'_fore_std'].append( format_forecast_nn(shape, shat[i,:], tlen, (ind, ts), False) )
        data[target+'_fore10'].append( format_forecast_nn(shape, yhat10[i,:], tlen, (ind, ts), log) )
        data[target+'_fore90'].append( format_forecast_nn(shape, yhat90[i,:], tlen, (ind, ts), log) )
    return data

def write2pg(data, name, target):
    from terraai_preprocessing.preprocessing.pp_main import establish_db_conn, write_DB
    # Format Taha's D_L to Aamir's D_L
    if target in ['oil', 'water']: tgtpd = target + '_pd'
    else: tgtpd = target 

    output = defaultdict(list)
    for item in tqdm(xrange(len(data['padID']))):
        tlen = len(data[target][item])
        output['API_prod'].append([data['padID'][item]]*tlen)
        output['prod_days'].append(data['proddays'][item])
        output['report_date_mo'].append(data['date'][item])
        output[tgtpd].append(data[target][item])
        output[tgtpd+'_fore_mean'].append(data[target+'_fore'][item])
        output[tgtpd+'_fore_std'].append(data[target+'_fore_std'][item])
        output[tgtpd+'_fore_p10'].append(data[target+'_fore10'][item])
        output[tgtpd+'_fore_p90'].append(data[target+'_fore90'][item])

    conn, cursor = establish_db_conn()
    write_DB(conn, cursor, output,
             verbosity=False, schema='results',
             table_name=name, drop_table=True)
    conn.close()

def write_sim_db(similars, setting):
    # Establish the connection
    conn = psycopg2.connect(database='terra', user=config['user'], password=config['password'], host=config['host'], port=config['port'])
    cursor = conn.cursor()
    query = """CREATE TABLE similar_wells.{} (target_well BIGINT, training_well BIGINT,
     seq_date DATE, similarity_score DOUBLE PRECISION, winp INT, winq INT);""".format(setting['table_name'])
    cursor.execute(query)

    for sim in tqdm(similars):
        query = """INSERT INTO similar_wells.{} VALUES {}""".format(setting['table_name'], sim)
        cursor.execute(query)

    conn.commit()
    cursor.close()
    conn.close()

def savePars2db(source, features, target, params, length, state): 
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()

    query = """INSERT INTO FFR_kernels VALUES ("{}","{}","{}",{},"{}","{}");""".format( \
        source, ','.join(features), target, length, ','.join("{0:.4f}".format(x) for x in params), state)
    cursor.execute(query)
    conn.commit()
    cursor.close()
    conn.close()

def checkKernelParindb(source, features, target, length, state): 
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM FFR_kernels;")
    row = cursor.fetchall()
    for item in row:
        conds = list()
        conds.append( source==item[0] )
        conds.append( target==item[2] )
        conds.append( length==int(item[3]) )
        # conds.append( state==item[5] )
        conds.append( set(features)==set(item[1].split(',')) )
        if reduce(lambda x,y: x and y, conds):
            params_db = [float(x) for x in item[4].split(',')]
            features_db = item[1].split(',')
            order = [features_db.index(x) for x in features]
            order += [o+len(features) for o in order]
            return [params_db[o] for o in order]+[params_db[-1]]
    return None

def read_result(well, table):
    query = """SELECT report_date_mo, oil_pd_fore_mean from results.{} where "API_prod"={}""".format(table, well)
    conn = psycopg2.connect(database='terra', user=config['user'], password=config['password'], host=config['host'], port=config['port'])
    cursor = conn.cursor()
    cursor.execute(query)
    row = cursor.fetchall()
    dates  = [diff_month_date(x[0]) for x in row]
    target = [mynum(float, x[1], np.nan) for x in row]
    dates, target = zip(*sorted(zip(dates, target)))
    cursor.close()
    conn.close()
    return dates, target

def write_extended_result(well, table, dates, target):
    dates = [add_months(x) for x in dates]
    conn = psycopg2.connect(database='terra', user=config['user'], password=config['password'], host=config['host'], port=config['port'])
    cursor = conn.cursor()
    values = ','.join(["({},{},'{}'::date)".format(well, target[i], dates[i]) for i in range(len(target))])
    query = """INSERT INTO results.{}("API_prod", oil_pd_fore_mean, report_date_mo) Values{};""".format(table, values)
    cursor.execute(query)
    conn.commit()
    cursor.close()
    conn.close()

def extend(well, table, setting):
    # Read the data
    dates, target = read_result(well, table)
    # Prepare data for linear regression
    n_fore = sum([~np.isnan(x) for x in target])
    n_train = min(n_fore, setting['max_len'])
    x_train = np.array(dates[-n_train:])
    y_train = np.array(target[-n_train:])
    x_test = dates[-1] + np.arange(1, setting['fore_len']+1)
    # Go to the log domain
    y_train = np.log10(y_train+1)
    y_train[~np.isfinite(y_train)] = 0
    y_pred = Ridge(alpha=1e-2).fit(x_train[:, None], y_train).predict(x_test[:, None])
    y_pred = np.power(10, y_pred)-1
    y_pred[y_pred<0] = 0
    # Write to db
    write_extended_result(well, table, x_test.tolist(), y_pred.tolist())

if __name__ == "__main__":
    # pdb.set_trace()
    # data = readFromDB()
    # params = np.array([  5.02493592,   0.30290206,   6.19230297,   2.20992589,
    #      2.11517852,   4.21499677,   0.07009884,   1.44529921,
    #      3.20938043,  17.86920671,   5.51474997,   0.22895883,   6.52829307])
    # # params = np.array([  0.05189916,  18.57176986,   1.18000321,   5.25568831,
    # #      0.05166955,   0.13827583,   2.04907352,   0.19549647,
    # #      0.06967513,   4.68506733,   4.36314379,   0.30220287,   0.15321623])
    # cc = abs(params[:6])/np.sum(abs(params[:6]))
    # params[:6] = cc
    # features = ['oil', 'water', 'proddays', 'location', 'depth', 'pool']
    # source = 'bigprod_ts'
    # target = 'oil'
    # length = 36
    # # savePars2db(source, features, target, params.tolist(), length, 'NDK')
    # print checkKernelParindb(source, features, target, length, 'NDK')
    # train_query = """Select "API_prod", report_date_mo, oil_mo, wtr_mo, prod_days_up, lat, longi, depth,
    #     pool_name from public_data.bigprod_ts where "API_prod" in (Select distinct "API_master" 
    #     from public_data.cali_master where field_name in ('Round Mountain', 'Poso Creek', 'Mount Poso', 'Kern Front', 'Kern River'));"""
    # test_query = """Select "API_prod", last_report_date from (Select "API_prod", max(report_date_mo) as last_report_date from public_data.big_prod_preprocess where "API_prod" in (Select distinct "API_master" from public_data.cali_master where 
    #     field_name in ('Round Mountain', 'Poso Creek', 'Mount Poso', 'Kern Front', 'Kern River')) group by "API_prod") a where a.last_report_date='2016-08-31' limit 100"""
    # read_from_db(train_query, test_query)
    # write_sim_db(0, 1)
    setting = {'max_len':20, 'fore_len':120}
    extend(33007009350000, '\"ffr_NDK_back_nn_oil\"', setting)

