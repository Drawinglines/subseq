from __future__ import division
import pdb
from copy import deepcopy
import cPickle as pk
from datetime import datetime
import numpy as np
from numpy.linalg import solve, inv
from scipy.spatial.distance import cdist
from tqdm import tqdm
import core_ml_nn as ml
import helpers_pg as hpf

class Updater(object):
    """
    This class is the implementation of a stochastic optimization algorithm.
    """
    params = None
    iteration = 0
    def __init__(self, init, lr, l2reg):
        self.lr = lr
        self.l2reg = l2reg
        self.d = len(init['params'][0])
        Updater.params = init

    def update(self, grad):
        """Simple SGD updates with a projection to positive values in the end."""
        Updater.iteration += 1
        lr = self.lr/np.power(Updater.iteration, 0.5)
        for k in range(self.d):
            Updater.params['params'][0][k] -= lr*(grad['params'][0][k]+self.l2reg*Updater.params['params'][0][k]) 
            Updater.params['params'][1][k] -= lr*(grad['params'][1][k]+self.l2reg*Updater.params['params'][1][k])
        Updater.params['sigma'][0] -= lr*(grad['sigma'][0]+self.l2reg*Updater.params['sigma'][0])
        Updater.params['sigma'][1] -= lr*(grad['sigma'][1]+self.l2reg*Updater.params['sigma'][1])
        self.project()

    def project(self, epsilon=1e-5):
        """This function projects the parameters to the positive quadrant."""
        for k in range(self.d):
            Updater.params['params'][0][k] = Updater.params['params'][0][k]*(Updater.params['params'][0][k] > 0)\
                                             + epsilon * (Updater.params['params'][0][k] <= 0)
            Updater.params['params'][1][k] = Updater.params['params'][1][k]*(Updater.params['params'][1][k] > 0)\
                                             + epsilon * (Updater.params['params'][1][k] <= 0)
        lb = 0.01 # For identifiability
        Updater.params['sigma'][1] = Updater.params['sigma'][1]*(Updater.params['sigma'][1] > lb)\
                                             + lb * (Updater.params['sigma'][1] <= lb)

def load_data():
    """
    Globally Loading the Data.
    It indends to improve efficiency of the algorithms that use the large `data` variable.
    """
    global data
    query = """Select "API_prod", report_date_mo, oil_pd, water_pd,gas_pd, prod_days_up, lat, longi,
               depth, pool_name from public_data.bigprod_ts where "state_API" = 33
               and depth is not null and pool_name is not null"""
    data, _ = hpf.read_from_db(query, None)
    # data = pk.load(open('dataRAND.pkl', 'rb'))

def preprocess(target):
    """ This function implements the most basic preprocessing for the data.
    It does the followings:
    # Makes sure that all of the values are finite.
    # Converts monthly oil and water to daily via dividing them by `proddays`.
    # Takes the log of oil and water.

    NOTE: This function is the only function that changes the global variable `data`
    after 'load_data()'.
    """
    if target not in ml.FEATURES.keys():
        ml.FEATURES[target] = 'ts'
    for i in range(len(data['oil'])):
        # For multi-dimensional forecasts
        if target is 'wor':
            data['wor'].append(data['water'][i]/data['oil'][i])
        elif target is 'cumoil':
            data['cumoil'][i].append(np.cumsum(data['oil'][i]))
        elif target is 'cumwater':
            data['cumwater'][i].append(np.cumsum(data['cumwater'][i]))
        # Main variables
        data['proddays'][i][np.isnan(data['proddays'][i])] = 0
        # Take log of oil
        # data['oil'][i] = np.log10(1+data['oil'][i])  # log(1+x) to make sure invertablity
        data['oil'][i][~np.isfinite(data['oil'][i])] = 0
        # data['water'][i] = np.log10(1+data['water'][i])
        data['water'][i][~np.isfinite(data['water'][i])] = 0
        # data['gas'][i] = np.log10(1+data['gas'][i])
        data['gas'][i][~np.isfinite(data['gas'][i])] = 0
        data[target][i][~np.isfinite(data[target][i])] = 0

def select_good_wells(opt_set):
    """
    This function select a set of wells that have not failed during training and testing periods.
    """
    test_date, winp, winq = opt_set['test_date'], opt_set['winp'], opt_set['winq']
    training = list()
    for i in range(len(data['padID'])):
        key_points = set([test_date-winp, test_date+winq, test_date, test_date-1])
        if key_points <= set(data['dates'][i]):
            t = np.where(data['dates'][i] == test_date-winp)[0][0]
            if sum(data['oil'][i][t:t+winp+winq] == 0)*1.0/(winq+winp) < 0.05:
                training.append((i, t))
    return training

def get_indexes(size, batch_size):
    """
    This function creates batches for the data.
    It randomly reshuffles the batches too.
    """
    ind = np.random.permutation(size) # Shuffling
    n_batch = int(size/batch_size)+1
    index = [ind[range(i*batch_size, min((i+1)*batch_size, size))] for i in range(n_batch)]
    return index

def kernel(we1, we2, params, setting):
    """
    Computes the kernel between two wells 'we1' and 'we2' given the parameters in 'params'.
    """
    winp = setting['winp']
    features = setting['features']
    out = 0
    grad = [[0 for _ in range(len(params[0]))] for _ in range(2)]
    for (j, item) in enumerate(features.keys()):
        if features[item] == 'ts':
            temp = cdist(data[item][we1[0]][we1[1]:we1[1]+winp][None, :], \
                data[item][we2[0]][we2[1]:we2[1]+winp][None, :])[0, 0]/np.sqrt(winp)
        elif features[item] == 'vector':
            temp = cdist(data[item][we1[0]][None, :], data[item][we2[0]][None, :])[0, 0]
        elif features[item] == 'scalar':
            temp = (data[item][we1[0]] - data[item][we2[0]])**2
        elif features[item] == 'category':
            temp = (data[item][we1[0]] != data[item][we2[0]])
        out += params[0][j]*np.exp(-temp*params[1][j])
        grad[0][j] = np.exp(-temp*params[1][j])
        grad[1][j] = -temp*params[0][j]*np.exp(-temp*params[1][j])
    return out, grad

def get_q(test_it, setting):
    """This function extracts the q sequences from time series.
    It also normalizes that by a particular mean of the winp period.
    """
    winp, winq, target = setting['winp'], setting['winq'], setting['target']
    qvector = list()
    for (i, t) in test_it:
        seq_p = data[target][i][t:t+winp]
        # nrm_fact = sum(seq_p)/(1+sum(seq_p!=0))  # This is the normalization factor that I need to try
        # seq_q = np.power(10, data[target][i][t+winp-1:t+winp+winq])-1
        seq_q = data[target][i][t+winp-1:t+winp+winq]
        # seq_q = seq_q/(1+np.power(10, nrm_fact))
        qvector.append(seq_q[1:]-seq_q[0])
    return np.vstack(qvector)

def fast_trace_mul(A, B):
    """A fast way of avoiding matrix multiplication"""
    return np.sum(A*B.T)

def likelihood(test_it, params, setting):
    """
    Returns the negative log-likelihood of observing the test_it under the Gaussian process
    model described by kernel parameters.
    """
    n, d = len(test_it), len(params['params'][0])
    Kxx = np.zeros((n, n))
    Kxx_p = [[np.zeros((n, n)) for _ in range(d)] for _ in range(2)]
    for i in range(n): #### This double loop can be done in parallel
        for j in range(i+1):
            kern, grad = kernel(test_it[i], test_it[j], params['params'], setting)
            Kxx[i, j] = kern
            Kxx[j, i] = kern
            for k in range(d):
                Kxx_p[0][k][i, j] = grad[0][k]
                Kxx_p[0][k][j, i] = grad[0][k]
                Kxx_p[1][k][i, j] = grad[1][k]
                Kxx_p[1][k][j, i] = grad[1][k]

    grad = {'params':[[0 for _ in range(d)] for _ in range(2)], 'sigma':[0, 0, 0]}
    qvector = get_q(test_it, setting)
    neg_log_like = 0
    for i in range(1, qvector.shape[1]):
        sigma = np.exp(params['sigma'][0] + params['sigma'][1]*i)
        Ky = Kxx+sigma*np.eye(n)
        Ky_s = [np.eye(n)*np.exp(params['sigma'][0] + params['sigma'][1]*i),
                np.eye(n)*i*np.exp(params['sigma'][0] + params['sigma'][1]*i)]
        alpha = solve(Ky, qvector[:, i])
        # The likelihood
        neg_log_like += np.dot(qvector[:, i], alpha)
        _, logdet = np.linalg.slogdet(Ky)
        neg_log_like += logdet
        # The gradient
        alpha = alpha[:, None]
        Beta = inv(Ky) - np.dot(alpha, alpha.T)
        for k in range(d):
            grad['params'][0][k] += fast_trace_mul(Beta, Kxx_p[0][k])
            grad['params'][1][k] += fast_trace_mul(Beta, Kxx_p[1][k])
        grad['sigma'][0] += fast_trace_mul(Beta, Ky_s[0])
        grad['sigma'][1] += fast_trace_mul(Beta, Ky_s[1])
    # Now scale them back by '/qvector.shape[1]/n'
    scale = qvector.shape[1]*n
    neg_log_like /= scale
    for k in range(d):
        grad['params'][0][k] /= scale
        grad['params'][1][k] /= scale
    grad['sigma'][0] /= scale
    grad['sigma'][1] /= scale
    return neg_log_like, grad

def fast_objective(test_it, params, setting):
    """
    This is an approximation of the true likelihood of all data,
    using batches.
    """
    objective = 0
    index = get_indexes(len(test_it), setting['batch_size'])
    print "Computing the objective in batches."
    for ind in tqdm(index):
        test = [test_it[x] for x in ind.tolist()]
        obj, _ = likelihood(test, params, setting)
        objective += obj*len(test)
    return objective/len(test_it)

def test_likelihood(test_it, params, setting):
    """
    This function numerically tests the accuracy of the computed gradient
    by likelihood() function.
    """
    delta = 1e-6
    like0, grad0 = likelihood(test_it, params, setting)
    # params['params'][0][2] += delta
    params['sigma'][0] += delta
    like1, _ = likelihood(test_it, params, setting)
    # print grad0['params'][0][2]
    print grad0['sigma'][0]
    print (like1-like0)/delta
    pdb.set_trace()

def optimize(opt_set):
    """
    Given the optimization settings in opt_set, this function
    finds the optimal kernel parameters by optimization over the
    randomly sampled dataset.
    """
    load_data()
    print "Loaded the data."
    opt_set['log'] = True if opt_set['target'] in ['oil', 'water'] else False
    preprocess(opt_set['target'])
    test_it = select_good_wells(opt_set)
    print "Number of training samples: {}".format(len(test_it))
    # test_likelihood(test_it[:50], opt_set['init'], opt_set)

    upd = Updater(opt_set['init'], opt_set['learn_rate'], opt_set['l2reg'])
    best_param, best_obj = Updater.params, np.inf
    for ep in range(opt_set['n_epoch']):
        index = get_indexes(len(test_it), opt_set['batch_size'])
        for ind in tqdm(index):
            test = [test_it[x] for x in ind.tolist()]
            _, grad = likelihood(test, Updater.params, opt_set)
            upd.update(grad)

        objective = fast_objective(test_it, Updater.params, opt_set)
        print "Epoch {0}: Negative Log-Likelihood: {1:.4f}".format(ep, objective)
        if objective < best_obj:
            best_param = deepcopy(Updater.params)
            best_obj = objective
    return best_param

if __name__ == "__main__":
    np.random.seed(0)
    PAR = {'params':[[0.1, 0.01, 0.01, 0.01, 0.1, 0.1, 0.1], [1, 1, 1, 1, 0.1, 0.1, 0.1]], 'sigma':[2, 0.1]}
    WINP = 30
    WINQ = 80
    TEST_DATE = hpf.diff_month(datetime.strptime('2009-01-01', '%Y-%m-%d'))
    OPT_SET = {'features':ml.FEATURES,
               'target':'oil',
               'winq':WINQ,
               'winp':WINP,
               'test_date':TEST_DATE,
               'batch_size':50,
               'n_epoch':50,
               'learn_rate':1e-2,
               'l2reg':0,
               'init':PAR}
    print optimize(OPT_SET)
