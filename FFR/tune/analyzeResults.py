import numpy as np 
import cPickle as pk 
import pdb

with open('random2.pkl', 'rb') as f:
	temp = pk.load(f)
	params = temp['params']
	results = temp['results']

params = np.array(params)
results = np.array(results)

pdb.set_trace()
ind = np.argsort(results)[:10]
print np.mean(params[ind,:], axis=0)
