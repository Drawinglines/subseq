from collections import defaultdict
import MySQLdb
from MySQLdb import OperationalError
import numpy as np
from datetime import datetime, date
from calendar import monthrange
import cPickle as pk
import pdb

initDate = datetime.strptime('1900-01-01' , '%Y-%m-%d')
indexDate = datetime.strptime('2008-01-01' , '%Y-%m-%d')

def diff_month(d1):
    try:
        return (d1.year - initDate.year)*12 + d1.month - initDate.month
    except:
        import pdb; pdb.set_trace()
        print "Exception.  The aregument was {}.".format(d1)
        return np.NaN

def add_months(months):
    month = initDate.month - 1 + months
    year = int(initDate.year + month / 12 )
    month = month % 12 + 1
    return date(year,month,monthrange(year,month)[1])  # Reporting on the last day of the month

def diff2Dates(din):
    return 

def mynum(func, a):
    ''' This function is only a way to get around the unexpected data.
    You may want to write a more sophisticated function for treatment of bad entries.'''
    try:
        return func(a)
    except:
        print('Expected float, got {} instead'.format(type(a)))
        return 0

def handleMissing(dates, oil, water, days):
	new_dates = np.arange(min(dates), max(dates)+1) 
	new_oil, new_water, new_days = [np.zeros(new_dates.shape) for _ in range(3)]
	new_oil[dates-min(dates)] = oil
	new_water[dates-min(dates)] = water
	new_days[dates-min(dates)] = days
	return new_dates, new_oil, new_water, new_days

def check_valid_test(data, ind=0):
    # If true, it can be used for testing
    deadline = diff_month(datetime.strptime('2009-01-01' , '%Y-%m-%d'))
    exm = dict()
    exm['dates'] = [str(add_months(x)) for x in data['dates'][ind] if x<deadline] 
    exm['oil'] =  [data['oil'][ind][i] for i in range(len(data['oil'][ind])) if data['dates'][ind][i]<deadline] 
    exm['water'] = [data['water'][ind][i] for i in range(len(data['water'][ind])) if data['dates'][ind][i]<deadline] 
    exm['proddays'] = [data['proddays'][ind][i] for i in range(len(data['proddays'][ind])) if data['dates'][ind][i]<deadline] 
    return Check_Valid_Producer(exm)[0] != 0  # if 0 => CF

def readFromDB():
    # Establish the connection
    cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com','user':'taha','password': 'taha@terraai','db': 'public_data'}
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()

    # query = "SELECT API_prod,dates,oil,water,proddays,spud_date,lat,longi FROM bigprod_ts WHERE state in (30, 33);"
    query = "SELECT API_prod,dates,oil,water,proddays,spud_date,lat,longi FROM bigprod_ts WHERE state=33;"
    cursor.execute(query)
    row = cursor.fetchall()
    data = defaultdict(list)
    # 
    cntr = 0
    for (i, item) in enumerate(row):
        # Exclude all-zero rows
        oil = np.array([mynum(float,x) for x in item[2].split(',')])
        if oil.sum() == 0:  continue
        
        dates  = np.array([diff_month(datetime.strptime(x, '%Y-%m-%d')) for x in  item[1].split(',')])
        index = np.argsort(dates)
        dates = dates[index]
        water = np.array([mynum(float,x) for x in item[3].split(',')])[index]
        days = np.array([mynum(int,x) for x in item[4].split(',')])[index]
        dates, oil, water, days = handleMissing(dates, oil[index], water, days)

        data['padID'].append( item[0] )
        data['dates'].append( dates )
        data['oil'].append( oil )
        data['water'].append( water )
        data['proddays'].append( days )
        data['year'].append( dates.min() )
        data['latitude'].append( item[6] )
        data['longitude'].append( item[7] )
        data['testable'].append( check_valid_test(data, cntr) )
        cntr += 1
        if i%1000 == 0:    print i     # This is a counter, because it takes a long time.

    # with open('NDKNMdata.p', 'wb') as f:
    with open('NDKdata.p', 'wb') as f:
        pk.dump(data, f)

    cursor.close()
    conn.close()
    return data

def convertData(dataIn, yhat, ind, win, lens):
    # Run this to convert the data to the right format for writing into the db
    inDate = diff_month(indexDate)
    data = defaultdict(list)
    for (i, jn) in enumerate(ind):
        data['padID'].append(dataIn['padID'][jn])
        data['date'].append( np.array( [add_months(x) for x in dataIn['dates'][jn].tolist()] ) )
        data['oil'].append(np.power(10, dataIn['oil'][jn])-1)
        data['water'].append(np.power(10, dataIn['water'][jn])-1) 
        water_fore = np.nan*np.empty(dataIn['water'][jn].shape)
        data['water_fore'].append(water_fore) 
        tind = np.where(dataIn['dates'][jn]==inDate)[0][0]
        thislen = dataIn['water'][jn].shape[0]
        oil_fore = np.nan*np.empty(dataIn['water'][jn].shape)
        oil_temp = np.power(10, yhat[i,:])-1
        oil_fore[tind:min(tind+win,thislen)] = (oil_temp*(oil_temp>=0))[:min(win,thislen-tind)]
        data['oil_fore'].append(oil_fore)
    return data

def formatForecast(shape, hat, ln, winp, jn):
    water_fore = np.nan*np.empty(shape)
    water_temp = np.power(10, hat[:ln])-1
    water_fore[jn[1]+winp:jn[1]+winp+ln] = (water_temp*(water_temp>=0))
    return water_fore

def confidence(yOhat, yWhat, shat):
    z = 1.645
    oil10 = yOhat - z*shat[:, None] 
    oil90 = yOhat + z*shat[:, None] 
    water10 = yWhat - z*shat[:, None] 
    water90 = yWhat + z*shat[:, None] 
    return oil10, oil90, water10, water90

def convertDataNew(dataIn, yOhat, yWhat, shat, ids, predLen, winp):
    # Run this to convert the data to the right format for writing into the db
    data = defaultdict(list)
    for (i, jn) in enumerate(ids):
        data['padID'].append(dataIn['padID'][jn[0]])
        data['date'].append( np.array( [add_months(x) for x in dataIn['dates'][jn[0]].tolist()] ) )
        data['oil'].append(np.power(10, dataIn['oil'][jn[0]])-1)
        data['proddays'].append(dataIn['proddays'][jn[0]])
        data['water'].append(np.power(10, dataIn['water'][jn[0]])-1) 

        shape = dataIn['water'][jn[0]].shape
        oil10, oil90, water10, water90 = confidence(yOhat, yWhat, shat)
        ss = np.log10(1+shat[i])*np.ones(yWhat[i,:].shape)  # This is a hack to be able to reuse the existing function
        data['water_fore'].append( formatForecast(shape, yWhat[i,:], predLen[i], winp, jn) ) 
        data['water_fore_std'].append( formatForecast(shape, ss, predLen[i], winp, jn) ) 
        data['water_fore10'].append( formatForecast(shape, water10[i,:], predLen[i], winp, jn) ) 
        data['water_fore90'].append( formatForecast(shape, water90[i,:], predLen[i], winp, jn) ) 
        data['oil_fore'].append( formatForecast(shape, yOhat[i,:], predLen[i], winp, jn) ) 
        data['oil_fore_std'].append( formatForecast(shape, ss, predLen[i], winp, jn) ) 
        data['oil_fore10'].append( formatForecast(shape, oil10[i,:], predLen[i], winp, jn) ) 
        data['oil_fore90'].append( formatForecast(shape, oil90[i,:], predLen[i], winp, jn) ) 
    return data

def write2db(data, name):
    # Establish the connection
    cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com',
            'user':'taha','password': 'taha@terraai','db': 'results'}    
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()
    
    # Drop table if exists
    query = "DROP TABLE IF EXISTS {};".format(name)
    cursor.execute(query)

    query = "CREATE TABLE {} (id varchar(32), date_ DATE,oil_pd DOUBLE,water_pd DOUBLE,oil_pd_forecast_mean DOUBLE,water_pd_forecast_mean DOUBLE);".format(name)
    cursor.execute(query)

    # Insert the rows
    values = list()
    for (i, well) in enumerate(data['padID']):
        for j in range(len(data['oil'][i])):
            values.append( "('{}','{}',{},{},{},{})".format(well, data['date'][i][j],data['oil'][i][j], data['water'][i][j], data['oil_fore'][i][j], data['water_fore'][i][j]))

    nBatch = 100    # Doing the queries in 100 batches
    stepSize = int(np.ceil(len(values)/nBatch))
    ind = [min(x*stepSize,len(values)) for x in range(nBatch)]
    for i in range(nBatch-1):
        query = 'INSERT INTO {} VALUES {};'.format(name,','.join(values[ind[i]:ind[i+1]]))
        query = query.replace('nan', 'NULL')
        query = query.replace('inf', 'NULL')
        cursor.execute(query)

    conn.commit()
    cursor.close()
    conn.close()

def write2db_new(data, name):
    # Establish the connection
    cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com',
            'user':'taha','password': 'taha@terraai','db': 'results'}    
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()
    
    # Drop table if exists
    query = "DROP TABLE IF EXISTS {};".format(name)
    cursor.execute(query)

    query = """CREATE TABLE {} (API_prod varchar(32), report_date_mo DATE, proddays INT, 
    oil_pd DOUBLE, oil_pd_fore_mean DOUBLE, oil_pd_fore_std DOUBLE, oil_pd_fore_p10 DOUBLE, oil_pd_fore_p90 DOUBLE, 
    wtr_pd DOUBLE, wtr_pd_fore_mean DOUBLE, wtr_pd_fore_std DOUBLE, wtr_pd_fore_p10 DOUBLE, wtr_pd_fore_p90 DOUBLE);""".format(name)
    cursor.execute(query)

    # Insert the rows
    values = list()
    for (i, well) in enumerate(data['padID']):
        for j in range(len(data['oil'][i])):
            values.append( "('{}','{}',{},{},{},{},{},{},{},{},{},{},{})".format(well, data['date'][i][j],data['proddays'][i][j], \
             data['oil'][i][j], data['oil_fore'][i][j], data['oil_fore_std'][i][j], data['oil_fore10'][i][j], data['oil_fore90'][i][j], \
             data['water'][i][j], data['water_fore'][i][j], data['water_fore_std'][i][j], data['water_fore10'][i][j], data['water_fore90'][i][j]))

    nBatch = 100    # Doing the queries in 100 batches
    stepSize = int(np.ceil(len(values)/nBatch))
    ind = [min(x*stepSize,len(values)) for x in range(nBatch)]
    for i in range(nBatch-1):
        query = 'INSERT INTO {} VALUES {};'.format(name,','.join(values[ind[i]:ind[i+1]]))
        query = query.replace('nan', 'NULL')
        query = query.replace('inf', 'NULL')
        cursor.execute(query)

    conn.commit()
    cursor.close()
    conn.close()

if __name__ == "__main__":
    # pdb.set_trace()
    data = readFromDB()


