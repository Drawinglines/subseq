from __future__ import division
from datetime import datetime
from collections import defaultdict
import cPickle as pk
import numpy as np
import helpers_ffr as hpf

TEST_DATE = hpf.diff_month(datetime.strptime('2009-01-01', '%Y-%m-%d'))
WINP = 36
WINQ = 84

def slice_data(data, ind):
    data_out = defaultdict(list)
    for item in ind:
        data_out['padID'].append(data['padID'][item])
        data_out['dates'].append(data['dates'][item])
        data_out['oil'].append(data['oil'][item])
        data_out['water'].append(data['water'][item])
        data_out['proddays'].append(data['proddays'][item])
        data_out['location'].append(data['location'][item])
        data_out['depth'].append(data['depth'][item])
        data_out['pool'].append(data['pool'][item])
    return data_out

def choose_good_wells(data, n_well=1000):
    prods = list()
    for i in range(len(data['oil'])):
        index = np.where(data['dates'][i] >= 1224)[0]
        tst_P = np.where(data['dates'][i]==TEST_DATE-WINP)[0]
        tst_Q = np.where(data['dates'][i]==TEST_DATE-WINQ)[0]
        tst_I = np.where(data['dates'][i]==TEST_DATE)[0]
        if (len(index) > 0) and (len(tst_P) > 0) and (len(tst_Q) > 0) and (len(tst_I) > 0):
            prods.append(sum(data['oil'][i][index]))
        else:
            prods.append(0)

    inds_sorted = np.argsort(np.array(prods))[::-1][:3*n_well]
    sel_ids = [inds_sorted[x] for x in np.random.permutation(len(inds_sorted)).tolist()]
    # Now select train and test sets
    data_train = slice_data(data, sel_ids[:n_well])
    data_valid = slice_data(data, sel_ids[n_well:2*n_well])
    data_test = slice_data(data, sel_ids[2*n_well:3*n_well])
    data_all = slice_data(data, sel_ids[:2*n_well])
    return data_train, data_test, data_valid, data_all

def main():
    data_all = hpf.readFromDB(stateID=[hpf.stateTable['NDK']])
    data_train, data_test, data_valid, data_all = choose_good_wells(data_all)
    with open('kernel_data.p', 'wb') as fh:
        pk.dump({'train':data_train, 'test':data_test, 'valid':data_valid, 'all':data_all}, fh)

if __name__ == "__main__":
    main()