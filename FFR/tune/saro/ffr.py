from __future__ import division
import warnings
from multiprocessing import Pool
import pdb
import numpy as np
from numpy.linalg import solve
from scipy.spatial.distance import cdist
from scipy.optimize import minimize

import helpers as hlp

warnings.filterwarnings("ignore")  # Not a very brilliant idea!

def kernel(train_data, test_data, pars):
    """
    This function finds the kernel between training_data and test_data given
    kernel parameters in pars.

    Please do not change the format of training and testing data.  The input
    parameter 'par' includes the coefficients used in computation of the kernel.
    """
    oil_tr, oil_ts = hlp.getx(train_data, 'oil'), hlp.getx(test_data, 'oil')
    wor_tr, wor_ts = hlp.getx(train_data, 'water'), hlp.getx(test_data, 'water')
    days_tr, days_ts = hlp.getx(train_data, 'proddays'), hlp.getx(test_data, 'proddays')
    # Compute the kernels
    KO = cdist(oil_tr, oil_ts)
    KW = cdist(wor_tr, wor_ts)
    KD = cdist(days_tr, days_ts)
    # The distance metric parameters
    return pars[0]/(pars[3] + KO) + pars[1]/(pars[4] + KW) + pars[2]/(pars[5] + KD)

def ffr(params, train_data, test_data):
    """
    This function runs FFR given the kernel parameters 'params' and the provided
    training and testing data.

    It returns the error on the test set as a scalar number.
    """
    sigma = params[-1]
    # Because scaling doesn't matter for the first dim coeffs
    dim = int((len(params)-1)/2)
    params[:dim] = [params[i]/sum(params[:dim]) for i in range(dim)]
    # Compute the kernels
    K = kernel(train_data, train_data, params[:-1])
    k = kernel(train_data, test_data, params[:-1])
    # Get the test data and perform the regression
    y_train, x_train = hlp.gety(train_data), hlp.getx(train_data, 'oil')
    y_test, x_test = hlp.gety(test_data), hlp.getx(test_data, 'oil')
    b_train = np.array([hlp.get_xlast(x_train[i, :]) for i in range(x_train.shape[0])])[:, None]
    b_test = np.array([hlp.get_xlast(x_test[i, :]) for i in range(x_test.shape[0])])[:, None]

    Kinvk = solve(K+sigma*np.eye(K.shape[0]), k)
    y_pred = np.dot(Kinvk.T, (y_train-b_train)) + b_test
    # Evaluation
    return hlp.evaluate_error(y_test, y_pred, 'median')

def ffr_call(inp):
    init, data = inp
    opt_res = minimize(ffr, init, data, method='Nelder-Mead', options={'maxiter':30})
    return opt_res['x'], opt_res['fun']

def opt_ffr(train_data, test_data, n_pars, n_try=8):
    """
    Given the training and testing set, this function finds the best parameters
    of the kernel.  It trys 'n_try' number of random initial points and reports
    the best result.

    This function returns optimal parameters 'params' as a list and the test error
    associated with it.
    """
    inputs = list()
    pool = Pool()
    for _ in range(n_try):
        init = np.exp(np.random.uniform(-3, 3, n_pars)).tolist()
        inputs.append((init, (train_data, test_data)))
    outputs = pool.map(ffr_call, inputs)
    pool.close()

    params = [x[0] for x in outputs]
    results = [x[1] for x in outputs]
    ind = np.argsort(results)[:1]
    params = np.mean(np.array(params)[ind, :], axis=0).tolist()
    return params, results[ind[0]]
# Main Function
if __name__ == "__main__":
    # Loading data
    tr_data, ts_data, vl_data, all_data = hlp.load_data('kernel_data.p')
    pdb.set_trace()
    # Example single run of FFR
    # pps = [0.636, 0.444, 0.503, 106.233, 473.606, 168.865, 0.032]
    # print ffr(pps, tr_data, ts_data)
    # Example of optimization of kernel parameters
    pps, res = opt_ffr(tr_data, vl_data, n_pars=7)
    print ffr(pps, all_data, ts_data)
