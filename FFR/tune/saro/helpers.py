from __future__ import division
import cPickle as pk
import numpy as np
from sklearn.linear_model import TheilSenRegressor

WINP = 36
WINQ = 84
TEST_DATE = 1308
SM = 10

def gety(data):
    n_data = len(data['oil'])
    y_data = np.zeros((n_data, WINQ))
    for i in range(n_data):
        index = np.where(data['dates'][i] == TEST_DATE)[0][0]
        tlen = min(len(data['oil'][i][index:]), WINP)
        y_data[i, :tlen] = data['oil'][i][index:index+tlen]
    return y_data

def getx(data, target):
    n_data = len(data[target])
    x_data = np.zeros((n_data, WINP))
    for i in range(n_data):
        index = np.where(data['dates'][i] == TEST_DATE)[0][0]
        tlen = min(len(data[target][i][:index]), WINP)
        x_data[i, -tlen:] = data[target][i][index-tlen:index]
    return x_data

def prep(data):
    for i in range(len(data['oil'])):
        data['proddays'][i][np.isnan(data['proddays'][i])] = 0
        data['oil'][i] /= data['proddays'][i]        # This makes oil per day (oil_pd)
        data['water'][i] /= data['proddays'][i]      # This makes water per day (water_pd)
        # Take log of oil
        data['oil'][i] = np.log10(1+data['oil'][i])  # log(1+x) to make sure that it is invertable
        data['oil'][i][~np.isfinite(data['oil'][i])] = 0
        data['water'][i] = np.log10(1+data['water'][i]) # log(1+x) to make sure invertablity
        data['water'][i][~np.isfinite(data['water'][i])] = 0
    return data

def load_data(filename):
    data = pk.load(open(filename, "rb"))
    return prep(data['train']), prep(data['test']), prep(data['valid']), prep(data['all'])

def evaluate_error(yture, yhat, measure='mean'):
    """
    Evaluation of the error of the forecasting.
    """
    errs = np.sum(((yhat-yture)**2)*(yture > 0), axis=1)
    norms = np.sum(yture**2, axis=1)
    nnz = np.where(norms != 0)[0]
    if measure == 'median':
        return np.median(np.sqrt(errs[nnz])/np.sqrt(norms[nnz]))
    elif measure == 'weighted':
        weights = norms/sum(norms)
        return np.sqrt(sum(weights[nnz]*errs[nnz]/norms[nnz]))
    else:
        return np.sqrt(np.mean(errs[nnz]/norms[nnz]))

def get_xlast(ys):
    """
    The goal of this function is to find a linear trend and estimate
    the next value of the time series. It uses TheilSenRegressor for robust regression.

    # ys is an array of n x 1
    """
    X = np.where(ys != 0)[0][:, None]
    y = ys[np.where(ys != 0)[0]][:, None]
    if sum(ys[-SM:]) == 0:  # Make stable estimation
        return sum(y[-SM:])/SM
    elif len(y) < SM:
        return 0
    est = TheilSenRegressor()
    est.fit(X, y[:, 0])
    x_last = est.coef_[0]*len(ys) + est.intercept_
    if x_last > 1.5*max(ys):
        x_last = max(ys)
    elif x_last < min(ys)-1:
        x_last = min(ys)
    return x_last
