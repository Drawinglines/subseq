# To Do #
1. Do the experiments with more data. (**Done**)
2. Study the impact of the following parameters:
     1. Window sizes
     2. Number of time series used for averaging
     3. The weights of oil, wor, and days
3. Vectorize the search over time. (**Later**)
4. Create the confidence intervals. (**Done**)
5. Implement formal evaluation.
6. Make sure that the evaluation is not done when the oil well fails. (**Done**)
7. Include the injections.
8. DTW and others.