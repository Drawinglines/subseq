function [rmse, signal] = forecastPiecewise
clc
clear
addpath(genpath('../funcs/'))
addpath(genpath('../../cvx/'))
load ../data/data4p.mat

lens = getLength(oil);
win = 84;

% Remove bad time series
index = find((Role == 0) | (lens < 150));
oil(index, :) = [];
lens(index) = [];

% Take the log of oil
zOil = (oil==0);
oil = log10(oil);
oil(zOil) = 0;

% Training and testing
n = size(oil, 1);
nTrain = 500;
ind = randperm(n);
indTrain = ind(1:nTrain);
indTest = ind(nTrain+1:end);

[lambda, sC] = validate(oil(indTrain, :), win, lens);

[rmse, signal] = doForecastPieceSet(oil, indTest, lambda, sC, win, lens);

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [fore, rmse, signal] = doForecastPiece(y, lambda, sC, win, lens)
% Fitting
y(isnan(y)) = 0;
[~, ~, pp] = l1tfRobust(y(1:lens-win)', lambda, sC);
% Forecasting
if pp(1, end) > 0
    pp(2, end) = pp(2, end) + (lens-win)*pp(1, end);
    pp(1, end) = 0;
end
fore = polyval(pp(:, end)', lens-win+1:lens);
rmse = norm(y(lens-win+1:lens)-fore);
signal = norm(y(lens-win+1:lens))^2;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [rmse, signal] = doForecastPieceSet(oil, index, lambda, sC, win, lens)
mse = 0;
signal = 0;
for i = 1:length(index)
    [~, mm, ss] = doForecastPiece(oil(index(i), 1:lens(index(i))), lambda, sC, win, lens(index(i)));
    mse = mse + mm^2;
    signal = signal + ss;
end
rmse = sqrt(mse/length(index));
signal = sqrt(signal/length(index));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [lambda, sC] = validate(oil, win, lens)
index = 1:size(oil, 1);
nL = 10;
lambdas = logspace(-2, 2, nL);
sCs = logspace(-4, 0, 6);
params = zeros(2, length(lambdas)*length(sCs));
for i = 1:length(sCs)
    params(2, nL*(i-1)+1:i*nL) = sCs(i);
    params(1, nL*(i-1)+1:i*nL) = lambdas;
end

result = inf*ones(size(params, 2), 1);
parfor i = 1:length(params(1, :))
    try
    result(i) = doForecastPieceSet(oil, index, params(1, i), params(2, i), win, lens);
    catch a
        disp(a)
    end
    disp(i)
end
[~, ix] = min(result);

lambda = params(1, ix(1));
sC = params(2, ix(1));
end






















