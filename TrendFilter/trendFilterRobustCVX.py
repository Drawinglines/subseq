#!/usr/bin/python
from numpy.linalg import norm, solve
import matplotlib.pyplot as plt
import cvxpy as cvx
import numpy as np
import csv

def lambdaMax(y):
    """ This function find the smallest lambda that makes the piecewise approximation
    a single line.
    """
    T = y.shape[0]
    I = np.eye(T-2)
    O = np.zeros((T-2,1))
    D = np.concatenate([I,O,O], axis=1)\
        + np.concatenate([O,-2*I,O], axis=1)\
        + np.concatenate([O,O,I], axis=1)
    return norm(solve(np.dot(D,D.transpose()), D*y), np.inf)
    
def tsSegment(x, D):
    """ This function computes the indexes of x in which a change point has happened.
    """
    z = np.abs(np.dot(D, x))        # Taking the second order difference of x
    th = 0.1
    z[ z < th*np.mean(z) ] = 0      # Removing the very small changes
    knots = np.nonzero(z)[0] + 1 # KRZ
    
    return knots

def robustL1trendFilter(y, lmbd):
    ''' This function find a piecewise linear trend for the input.
    Inputs: y: the original (N,) time series;  lmbd: the regularization parameter.
    Outputs: x: the trend;  segs: the change points in the time series.
    '''
    lam = lambdaMax(y)*lmbd
    T = y.shape[0]
    # Create a tridiagonal matrix
    D = np.concatenate( [np.diag(np.ones((T-2,))), np.zeros((T-2,2))], axis=1) \
        + np.concatenate( [np.zeros((T-2,1)), np.diag(-2*np.ones((T-2,))), np.zeros((T-2,1))], axis=1) \
        + np.concatenate( [np.zeros((T-2,2)), np.diag(np.ones((T-2,)))], axis=1)
    Z = np.diag(1.0*(y!=0))

    x = cvx.Variable(T)
    objective = cvx.Minimize(cvx.norm(Z*x - y, 1) + lam*cvx.norm(D*x,1))
    prob = cvx.Problem(objective)
    prob.solve(solver=cvx.CVXOPT)
    
    # Now segment the time series
    segs = tsSegment(x.value, D)
    
    return x.value, segs

## Main Function
oil = np.random.standard_normal((120,))

x =  robustL1trendFilter(oil, 0.05)[0]
plt.plot(oil)   # KRZ
plt.plot(x, 'r') # KRZ
plt.show()
