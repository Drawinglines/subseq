import numpy as np
from numpy.linalg import norm
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.advanced_activations import ELU
from sklearn.linear_model import Ridge
from sklearn.preprocessing import scale 
import helpers_agg as hpa 
import cPickle as pk
import pdb

# The following function should be put in `objectives.py` of Keras.
def ensemble(y_true, y_pred):
    return K.mean(K.square(y_true[:,0]- K.sum(y_pred*y_true[:,1:],axis=-1)))

def getIndexes(n, rTrain):
    np.random.seed(0)
    nT = int(rTrain*n)
    index = np.random.permutation(n)
    return index[:nT], index[nT:]

def preprocessStaticFeatures(dataIn): 
    data = np.array(dataIn, dtype=np.float)
    data[~np.isfinite(data)] = 0
    return scale( data ) 

def simpleAggregate(resultTables, dataTable, outputTable, rTrain=0.3):
    def trainSimple(data, ind):
        X, y = list(), list()
        for jn in ind:
            item = data[jn]
            if len(item) > 0:
                index = np.where(np.isfinite(item[0, :]) & (item[0,:]!=0))[0]
                y.append(item[0, index][:, np.newaxis])
                X.append(item[1:, index].T)
        X = np.vstack(tuple(X))
        X = np.hstack((X,np.ones((X.shape[0],1))))  # Adding another simple all ones algorithm
        y = np.vstack(tuple(y))
        # w,_ = modelLinear(X, y)
        clf = Ridge(alpha=0.0001, fit_intercept=False)
        return clf.fit(X, y).coef_
    def testSimple(data, ind, w):
        yhat, pid, allerr = list(), list(), list()
        for jn in ind:
            item = data['results'][jn]
            if len(item) > 0:
                pid.append(data['id'][jn])
                X = np.vstack((item[1:, :], np.ones((1, item.shape[1]))))
                yhat.append(np.dot(w,X)[0,:])
                allerr += (yhat[-1] - item[0:1, :])[0,:].tolist()
        allerr = np.array(allerr)
        print norm(allerr[np.isfinite(allerr)])/np.sqrt(allerr.shape[0])
        return yhat, pid
    # Main body of the function
    data = hpa.getDataAgg(resultTables, dataTable)
    inTrain, inTest = getIndexes(len(data['id']), rTrain)
    w = trainSimple(data['results'], inTrain)
    yhat, pid = testSimple(data, inTest, w)
    dataOut = hpa.convertData(dataTable, yhat, pid)
    hpa.write2db(dataOut, outputTable)

def nnAggregate(resultTables, dataTable, outputTable, rTrain=0.3):
    def trainNN(data, features, ind): 
        # Prepare data
        X, z = list(), list()
        for (i, jn) in enumerate(ind):
            item = data[jn]
            if len(item) > 0:
                index = np.where(np.isfinite(np.sum(item,axis=0)) & (item[0,:]!=0))[0]
                X.append(item[:, index].T)
                z.append(np.tile(features[i:i+1,:].T, len(index)).T)
        X = np.vstack(tuple(X))
        z = np.vstack(tuple(z))
        X = np.hstack((X,np.ones((X.shape[0],1))))  

        # Build and fit model
        doRate = 0.95
        nIn, nOut = z.shape[1], X.shape[1]-1
        model = Sequential()
        model.add(Dense(output_dim=15, input_dim=nIn, init="glorot_uniform", activation="tanh"))
        model.add(Dropout(doRate))
        model.add(Dense(output_dim=5, init="glorot_uniform", activation="tanh"))
        model.add(Dropout(doRate))
        model.add(Dense(output_dim=nOut, init="glorot_uniform", activation=ELU()))
        model.compile(loss='ensemble', optimizer='rmsprop')
        model.fit(z,X, nb_epoch=100)
        return model

    def testNN(model, data, features, ind):
        weights = model.predict(features[ind,:])
        yhat, pid, allerr = list(), list(), list()
        for (i,jn) in enumerate(ind):
            item = data['results'][jn]
            if len(item) > 0:
                pid.append(data['id'][jn])
                X = np.vstack((item[1:, :], np.ones((1, item.shape[1]))))
                yhat.append(np.dot(weights[i:i+1,:],X)[0,:])
                allerr += (yhat[-1] - item[0:1, :])[0,:].tolist()
        allerr = np.array(allerr)
        print norm(allerr[np.isfinite(allerr)])/np.sqrt(allerr.shape[0])
        return yhat, pid

    data = hpa.getDataAgg(resultTables, dataTable)
    inTrain, inTest = getIndexes(len(data['id']), rTrain)
    features = preprocessStaticFeatures(data['static'])   
    model = trainNN(data['results'], features, inTrain)  
    yhat, pid = testNN(model, data, features, inTest)  
    dataOut = hpa.convertData(dataTable, yhat, pid)
    hpa.write2db(dataOut, outputTable)

if __name__ == "__main__":
    resultTables = ['GP_l80_W32_N5_FULL_ND', 'FFR_7_6_300_indexDate']
    dataTable = 'bigprod_ts'
    # outputTable = "AGG_simple"
    # simpleAggregate(resultTables, dataTable, outputTable)
    outputTable = "AGG_NN"
    nnAggregate(resultTables, dataTable, outputTable)
