from __future__ import division
from collections import defaultdict
import MySQLdb
import numpy as np
from datetime import datetime, 
from calendar import monthrange
import cPickle as pk
import pdb

initDate = datetime.strptime('1900-01-01' , '%Y-%m-%d')
indexDate = datetime.strptime('2008-01-01' , '%Y-%m-%d')

def diff_month(d1):
    try:
        return (d1.year - initDate.year)*12 + d1.month - initDate.month
    except:
        import pdb; pdb.set_trace()
        print "Exception.  The aregument was {}.".format(d1)
        return np.NaN

def add_months(months):
    month = initDate.month - 1 + months
    year = int(initDate.year + month / 12 )
    month = month % 12 + 1
    return date(year,month,monthrange(year,month)[1])  # Reporting on the last day of the month

def mynum(func, a):
    ''' This function is only a way to get around the unexpected data.
    You may want to write a more sophisticated function for treatment of bad entries.'''
    try:
        return func(a)
    except:
        print('Expected float, got {} instead'.format(type(a)))
        return 0

def handleMissing(dates, oil, water, days):
    new_dates = np.arange(min(dates), max(dates)+1) 
    new_oil, new_water, new_days = [np.zeros(new_dates.shape) for _ in range(3)]
    new_oil[dates-min(dates)] = oil
    new_water[dates-min(dates)] = water
    new_days[dates-min(dates)] = days
    return new_dates, new_oil, new_water, new_days

def getForecastTable(tableName):
    ''' Returns a defaultdict of numpy arrays
    '''
    cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com','user':'taha','password': 'taha@terraai','db': 'results'}
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()
    query = """SELECT id, group_concat(date_), group_concat(oil_pd_forecast_mean) FROM {} WHERE `oil_pd_forecast_mean` is not NULL GROUP BY id;""".format(tableName, indexDate)
    cursor.execute(query)
    row = cursor.fetchall()
    data = defaultdict(list)
    for item in row:
        data['padID'].append( mynum(long,item[0]) )
        dates  = np.array([diff_month(datetime.strptime(x, '%Y-%m-%d')) for x in  item[1].split(',')])
        oil = np.array([mynum(float,x) for x in item[2].split(',')])
        index = np.argsort(dates)
        data['data'].append(np.vstack((dates[index],oil[index])).T)
    cursor.close()
    conn.close()
    return data

def getData(dataTable):
    ''' Returns a defaultdict of data in the old format.
    '''
    cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com','user':'taha','password': 'taha@terraai','db': 'public_data'}
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()

    query = "SELECT API_prod,dates,oil,water,proddays,spud_date,lat,longi FROM bigprod_ts  WHERE API_prod in (SELECT id FROM results.test_ids_NDK);"
    cursor.execute(query)
    row = cursor.fetchall()
    data = defaultdict(list)
    for (i, item) in enumerate(row):
        # Exclude all-zero rows
        oil = np.array([mynum(float,x) for x in item[2].split(',')])
        if oil.sum() == 0:  continue
        
        dates  = np.array([diff_month(datetime.strptime(x, '%Y-%m-%d')) for x in  item[1].split(',')])
        index = np.argsort(dates)
        dates = dates[index]
        water = np.array([mynum(float,x) for x in item[3].split(',')])[index]
        days = np.array([mynum(int,x) for x in item[4].split(',')])[index]
        dates, oil, water, days = handleMissing(dates, oil[index], water, days)

        data['padID'].append( item[0] )
        data['dates'].append( dates )
        data['oil'].append( oil )
        data['water'].append( water )
        data['proddays'].append( days )
        data['year'].append( dates.min() )
        data['latitude'].append( item[6] )
        data['longitude'].append( item[7] )

    cursor.close()
    conn.close() 
    
    return data

def getDataAgg(resultTables, dataTable, pd=True):
    # Get the data
    fore = [getForecastTable(x) for x in resultTables]
    data = getData(dataTable)

    # Align the data
    inDate = diff_month(indexDate)  
    dataOut = defaultdict(list)
    for ii in data['padID']:
        # ID
        dataOut['id'].append(ii)
        ind = data['padID'].index(ii)
        tind = np.where(data['dates'][ind]==inDate)[0][0]
        # Static features
        dataOut['static'].append(np.array([data['year'][ind], data['latitude'][ind], data['longitude'][ind], inDate-min(data['dates'][ind])]))
        # Dynamic features
        dataOut['dynamic'].append( np.vstack((data['oil'][ind][:tind], data['water'][ind][:tind],\
         data['proddays'][ind][:tind], data['dates'][ind][:tind])) )
        # Results
        try:   # Technically this should not ever happen.
            results = [data['oil'][ind][tind:]/data['proddays'][ind][tind:]]
            # days = data['proddays'][ind][tind:] if pd else np.ones(data['proddays'][ind][tind:].shape)
            for (alg, item) in enumerate(fore):
                jj = item['padID'].index(ii)
                tind = np.where(item['data'][jj][:,0]==inDate)[0][0]
                results.append(item['data'][jj][tind:, 1])
            minLen = min([x.shape[0] for x in results])
            dataOut['results'].append(np.vstack(tuple([np.log10(1+x[:minLen]) for x in results])))
        except:
            dataOut['results'].append([])
            print "Item {}, {}".format(data['padID'].index(ii), alg)
    return dataOut

def convertData(dataTable, yhat, padIDs):
    # Run this to convert the data to the right format for writing into the db
    dataIn = getData(dataTable)
    inDate = diff_month(indexDate)
    data = defaultdict(list)
    for (i, pid) in enumerate(padIDs):
        jn = dataIn['padID'].index(pid)
        data['padID'].append(dataIn['padID'][jn])
        data['date'].append( np.array( [add_months(x) for x in dataIn['dates'][jn].tolist()] ) )
        data['oil'].append( dataIn['oil'][jn]/dataIn['proddays'][jn] )
        data['water'].append(dataIn['water'][jn]) 
        water_fore = np.nan*np.empty(dataIn['dates'][jn].shape)
        data['water_fore'].append(water_fore) 
        tind = np.where(dataIn['dates'][jn]==inDate)[0][0]
        thislen = yhat[i].shape[0]
        oil_fore = np.nan*np.empty(dataIn['dates'][jn].shape)
        oil_temp = np.power(10, yhat[i])-1
        oil_fore[tind:tind+thislen] = (oil_temp*(oil_temp>=0))
        data['oil_fore'].append(oil_fore)
    return data

def write2db(data, name):
    # Establish the connection
    cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com',
            'user':'taha','password': 'taha@terraai','db': 'results'}    
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()
    
    # Drop table if exists
    query = "DROP TABLE IF EXISTS {};".format(name)
    cursor.execute(query)

    query = "CREATE TABLE {} (id varchar(32), date_ DATE,oil_pd DOUBLE,water_pd DOUBLE,oil_pd_forecast_mean DOUBLE,water_pd_forecast_mean DOUBLE);".format(name)
    cursor.execute(query)

    # Insert the rows
    values = list()
    for (i, well) in enumerate(data['padID']):
        for j in range(len(data['oil'][i])):
            values.append( "('{}','{}',{},{},{},{})".format(well, data['date'][i][j],data['oil'][i][j], data['water'][i][j], data['oil_fore'][i][j], data['water_fore'][i][j]))

    nBatch = 100    # Doing the queries in 100 batches
    stepSize = int(np.ceil(len(values)/nBatch))
    ind = [min(x*stepSize,len(values)) for x in range(nBatch)]
    for i in range(nBatch-1):
        query = 'INSERT INTO {} VALUES {};'.format(name,','.join(values[ind[i]:ind[i+1]]))
        query = query.replace('nan', 'NULL')
        query = query.replace('inf', 'NULL')
        cursor.execute(query)

    conn.commit()
    cursor.close()
    conn.close()

if __name__ == "__main__":
    resultTables = ['GP_l80_W32_N5_FULL_ND', 'FFR_7_6_300_indexDate']
    dataTable = 'bigprod_ts'
    data = getDataAgg(resultTables, dataTable)
    pdb.set_trace()