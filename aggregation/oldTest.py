import cvxpy as cvx
import numpy as np
from numpy.linalg import norm
import helpers_agg as hpa 
import cPickle as pk
import pdb

def modelAggregate(X, y):
	''' This function finds the weights for different algorithms based on their accuracy in predcition.
	Input:  y: nx1, where "n" is the size of the validation dataset.  
	-----   X: nxp, where "p" is the number of algorithms.
	-----   We assume that every column of X includes the prediction of 'y' by the corresponding algorithm.
	Output: w: px1, where 'w[i]' indicates the weight assigned to 
	'''
	w = cvx.Variable(X.shape[1])
	obj = cvx.Minimize( cvx.sum_squares(X*w - y) )
	constraints = [w >= 0] # cvx.sum_entries(w) == 1, 
	prob = cvx.Problem(obj, constraints)
	prob.solve(solver=cvx.CVXOPT, verbose=True)  # Set "verbose=True" for debugging
	return w.value, prob.value

def simpleAggregate(nT=10000):
    wellIDs = 'test_ids_33.csv'
    resultTables = ['GP_l80_W32_N5_FULL_ND', 'FFR_7_6_300_indexDate']
    dataTable = 'bigprod_ts'
    data = hpa.getDataAgg(resultTables, dataTable, wellIDs)['results']
    # pk.dump(data, open('dataAgg.p', 'wb'))
    X, y = list(), list()
    for item in data:
        if len(item) > 0:
        	index = np.where(np.isfinite(item[0, :]) & (item[0,:]!=0))[0]
        	y.append(item[0, index][:, np.newaxis])
        	X.append(item[1:, index].T)
    X = np.vstack(tuple(X))
    X = np.hstack((X,np.ones((X.shape[0],1))))  # Adding another simple all ones algorithm
    y = np.vstack(tuple(y))
    # Use the first nT to learn the coefficients
    np.random.seed(0)
    index = np.random.permutation(X.shape[0])
    inTrain = index[:nT]
    inTest = index[nT:]
    w,_ = modelAggregate(X[inTrain,:], y[inTrain])
    print w
    nrm = norm(y[inTest,0])  # For normalization of the errors
    print "GP:       {}".format( norm(X[inTest,0]-y[inTest,0])/nrm )
    print "FFR:      {}".format( norm(X[inTest,1]-y[inTest,0])/nrm )
    print "Ensemble: {}".format( norm(np.dot(X[inTest,:],w)-y[inTest])/nrm )

if __name__ == "__main__":
	simpleAggregate()